/*******************************************************************************
 * Size: 18 px
 * Bpp: 1
 * Opts: --bpp 1 --size 18 --font D:\projects\inv_display\SquareLine\assets\Roboto-Regular.ttf -o D:\projects\inv_display\SquareLine\assets\ui_font_RobotoBold16.c --format lvgl -r 0x20-0x7f --symbols АаБбВвГгҐґДдЕеЄєЖжЗзИиІіЇїЙйКкЛлМмНнОоПпРрСсТтУуФфХхЦцЧчШшЩщьЮюЯя --no-compress --no-prefilter
 ******************************************************************************/

#include "ui.h"

#ifndef UI_FONT_ROBOTOBOLD16
#define UI_FONT_ROBOTOBOLD16 1
#endif

#if UI_FONT_ROBOTOBOLD16

/*-----------------
 *    BITMAPS
 *----------------*/

/*Store the image of the glyphs*/
static LV_ATTRIBUTE_LARGE_CONST const uint8_t glyph_bitmap[] = {
    /* U+0020 " " */
    0x0,

    /* U+0021 "!" */
    0xff, 0xff, 0xc3, 0xc0,

    /* U+0022 "\"" */
    0xb6, 0xd0,

    /* U+0023 "#" */
    0x9, 0x82, 0x41, 0x91, 0xff, 0x13, 0x4, 0x81,
    0x20, 0x48, 0xff, 0x88, 0x82, 0x60, 0x98, 0x24,
    0x0,

    /* U+0024 "$" */
    0x18, 0x18, 0x7e, 0xe7, 0xc3, 0xc3, 0xc0, 0x70,
    0x3c, 0xe, 0x7, 0xc3, 0xc3, 0xe7, 0x7e, 0x18,
    0x18,

    /* U+0025 "%" */
    0x70, 0x11, 0x2, 0x26, 0x44, 0x88, 0xa0, 0xec,
    0x1, 0x0, 0x6e, 0xa, 0x22, 0x44, 0xc8, 0x91,
    0x10, 0x1c,

    /* U+0026 "&" */
    0x3e, 0xc, 0x61, 0x8c, 0x31, 0x87, 0x60, 0x78,
    0xe, 0x3, 0x66, 0xc6, 0xd8, 0x73, 0x6, 0x31,
    0xc3, 0xec,

    /* U+0027 "'" */
    0xf0,

    /* U+0028 "(" */
    0x8, 0xcc, 0xc6, 0x23, 0x18, 0xc6, 0x31, 0x8c,
    0x21, 0x8c, 0x30, 0x82,

    /* U+0029 ")" */
    0x86, 0x18, 0x43, 0x18, 0x63, 0x18, 0xc6, 0x31,
    0x88, 0xc6, 0x62, 0x20,

    /* U+002A "*" */
    0x18, 0x18, 0x99, 0xff, 0x38, 0x3c, 0x66, 0x0,

    /* U+002B "+" */
    0x18, 0xc, 0x6, 0x3, 0xf, 0xf8, 0xc0, 0x60,
    0x30, 0x18, 0x0,

    /* U+002C "," */
    0x6d, 0xe0,

    /* U+002D "-" */
    0xf0,

    /* U+002E "." */
    0xf0,

    /* U+002F "/" */
    0x6, 0x8, 0x10, 0x60, 0x83, 0x6, 0x8, 0x30,
    0x40, 0x83, 0x4, 0x8, 0x30, 0x0,

    /* U+0030 "0" */
    0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xc3, 0xc3, 0xc3, 0x66, 0x3c,

    /* U+0031 "1" */
    0xd, 0xfe, 0xc3, 0xc, 0x30, 0xc3, 0xc, 0x30,
    0xc3, 0xc,

    /* U+0032 "2" */
    0x3c, 0x66, 0xc3, 0xc3, 0x3, 0x7, 0x6, 0xc,
    0x18, 0x38, 0x70, 0x60, 0xff,

    /* U+0033 "3" */
    0x3c, 0x66, 0xc3, 0x3, 0x3, 0x6, 0x1c, 0x6,
    0x3, 0x3, 0xc3, 0x66, 0x3c,

    /* U+0034 "4" */
    0x3, 0x3, 0x83, 0xc1, 0x61, 0xb1, 0x98, 0x8c,
    0xc6, 0xc3, 0x7f, 0xc0, 0xc0, 0x60, 0x30,

    /* U+0035 "5" */
    0x7f, 0x40, 0xc0, 0xc0, 0xfc, 0xc6, 0x3, 0x3,
    0x3, 0x83, 0x83, 0xc6, 0x3c,

    /* U+0036 "6" */
    0x1c, 0x30, 0x60, 0x40, 0xdc, 0xe6, 0xc3, 0xc3,
    0xc3, 0xc3, 0xc3, 0x66, 0x3c,

    /* U+0037 "7" */
    0xff, 0x80, 0xc0, 0x40, 0x60, 0x20, 0x30, 0x10,
    0x18, 0xc, 0xc, 0x6, 0x6, 0x3, 0x0,

    /* U+0038 "8" */
    0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0x66, 0x3c, 0x66,
    0xc3, 0xc3, 0xc3, 0x66, 0x3c,

    /* U+0039 "9" */
    0x3c, 0x66, 0xc3, 0xc3, 0xc3, 0xc3, 0x67, 0x3b,
    0x3, 0x3, 0x6, 0xc, 0x38,

    /* U+003A ":" */
    0xf0, 0x0, 0xf0,

    /* U+003B ";" */
    0x6c, 0x0, 0x0, 0x6d, 0xa4,

    /* U+003C "<" */
    0x2, 0x1c, 0xe7, 0xc, 0xf, 0x7, 0x83,

    /* U+003D "=" */
    0xfe, 0x0, 0x0, 0xf, 0xe0,

    /* U+003E ">" */
    0x80, 0xe0, 0x38, 0xf, 0x7, 0x3c, 0xf0, 0xc0,

    /* U+003F "?" */
    0x7d, 0x8f, 0x18, 0x30, 0x61, 0x86, 0x18, 0x30,
    0x0, 0x1, 0x83, 0x0,

    /* U+0040 "@" */
    0x7, 0xc0, 0x60, 0xc3, 0x1, 0x98, 0x2, 0x63,
    0xcb, 0x19, 0x1c, 0x44, 0x73, 0x11, 0xc8, 0x47,
    0x21, 0x1c, 0x8c, 0x73, 0x32, 0x67, 0x71, 0x80,
    0x3, 0x0, 0xe, 0x10, 0xf, 0xc0,

    /* U+0041 "A" */
    0x6, 0x0, 0x60, 0xf, 0x0, 0xb0, 0x19, 0x1,
    0x98, 0x11, 0x83, 0x8, 0x3f, 0xc2, 0x4, 0x60,
    0x64, 0x6, 0xc0, 0x20,

    /* U+0042 "B" */
    0xfe, 0x61, 0xb0, 0x78, 0x3c, 0x1e, 0x1b, 0xf9,
    0x86, 0xc1, 0xe0, 0xf0, 0x78, 0x6f, 0xe0,

    /* U+0043 "C" */
    0x1f, 0xc, 0x66, 0xf, 0x3, 0xc0, 0x30, 0xc,
    0x3, 0x0, 0xc0, 0x30, 0x36, 0xd, 0xc6, 0x1f,
    0x0,

    /* U+0044 "D" */
    0xfc, 0x63, 0x30, 0xd8, 0x3c, 0x1e, 0xf, 0x7,
    0x83, 0xc1, 0xe0, 0xf0, 0xd8, 0xcf, 0xc0,

    /* U+0045 "E" */
    0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xff, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xff,

    /* U+0046 "F" */
    0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xfe, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xc0,

    /* U+0047 "G" */
    0x1f, 0x1c, 0x66, 0xf, 0x0, 0xc0, 0x30, 0xc,
    0x7f, 0x3, 0xc0, 0xf0, 0x36, 0xc, 0xc7, 0x1f,
    0x0,

    /* U+0048 "H" */
    0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0, 0xf0, 0x3f,
    0xff, 0x3, 0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0,
    0xc0,

    /* U+0049 "I" */
    0xff, 0xff, 0xff, 0xc0,

    /* U+004A "J" */
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3,
    0x3, 0x3, 0xc3, 0xe6, 0x3c,

    /* U+004B "K" */
    0xc1, 0xb0, 0xcc, 0x73, 0x18, 0xcc, 0x36, 0xf,
    0x83, 0xb0, 0xc6, 0x31, 0xcc, 0x33, 0x6, 0xc0,
    0xc0,

    /* U+004C "L" */
    0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xff,

    /* U+004D "M" */
    0xe0, 0x3f, 0x1, 0xf8, 0xf, 0xe0, 0xfd, 0x5,
    0xec, 0x6f, 0x63, 0x79, 0x13, 0xcd, 0x9e, 0x28,
    0xf1, 0x47, 0x8e, 0x3c, 0x21, 0x80,

    /* U+004E "N" */
    0xc0, 0xf8, 0x3e, 0xf, 0xc3, 0xd8, 0xf6, 0x3c,
    0xcf, 0x13, 0xc6, 0xf0, 0xfc, 0x3f, 0x7, 0xc0,
    0xc0,

    /* U+004F "O" */
    0x1e, 0xc, 0xe6, 0x1b, 0x3, 0xc0, 0xf0, 0x3c,
    0xf, 0x3, 0xc0, 0xf0, 0x36, 0x19, 0xc6, 0x1e,
    0x0,

    /* U+0050 "P" */
    0xfe, 0x61, 0xb0, 0x78, 0x3c, 0x1e, 0xf, 0xd,
    0xfc, 0xc0, 0x60, 0x30, 0x18, 0xc, 0x0,

    /* U+0051 "Q" */
    0x1e, 0xc, 0xc6, 0x1b, 0x3, 0xc0, 0xf0, 0x3c,
    0xf, 0x3, 0xc0, 0xf0, 0x36, 0x19, 0xce, 0x1f,
    0x0, 0x60, 0xc,

    /* U+0052 "R" */
    0xfe, 0x30, 0xcc, 0x1b, 0x6, 0xc1, 0xb0, 0xcf,
    0xe3, 0x18, 0xc6, 0x30, 0xcc, 0x33, 0x6, 0xc1,
    0x80,

    /* U+0053 "S" */
    0x3e, 0x31, 0xb0, 0x78, 0x3e, 0x3, 0xc0, 0xf8,
    0x1e, 0x3, 0xe0, 0xf0, 0x6c, 0x63, 0xe0,

    /* U+0054 "T" */
    0xff, 0xc3, 0x0, 0xc0, 0x30, 0xc, 0x3, 0x0,
    0xc0, 0x30, 0xc, 0x3, 0x0, 0xc0, 0x30, 0xc,
    0x0,

    /* U+0055 "U" */
    0xc1, 0xe0, 0xf0, 0x78, 0x3c, 0x1e, 0xf, 0x7,
    0x83, 0xc1, 0xe0, 0xf0, 0x6c, 0x63, 0xe0,

    /* U+0056 "V" */
    0xc0, 0x6c, 0xd, 0x81, 0x10, 0x63, 0xc, 0x61,
    0x4, 0x60, 0xc8, 0x1b, 0x1, 0x60, 0x38, 0x7,
    0x0, 0x60,

    /* U+0057 "W" */
    0x83, 0x5, 0x86, 0xb, 0xc, 0x36, 0x3c, 0x64,
    0x68, 0xc8, 0x91, 0x19, 0x36, 0x36, 0x6c, 0x28,
    0x58, 0x50, 0xa0, 0xa1, 0xc1, 0xc1, 0x83, 0x3,
    0x0,

    /* U+0058 "X" */
    0x60, 0xc4, 0x18, 0xc6, 0xc, 0xc1, 0xb0, 0x1c,
    0x1, 0x80, 0x70, 0x1b, 0x3, 0x30, 0xc6, 0x30,
    0x66, 0xe,

    /* U+0059 "Y" */
    0xc0, 0xf0, 0x36, 0x19, 0x86, 0x33, 0x4, 0x81,
    0xe0, 0x30, 0xc, 0x3, 0x0, 0xc0, 0x30, 0xc,
    0x0,

    /* U+005A "Z" */
    0xff, 0x80, 0xc0, 0xc0, 0xc0, 0x60, 0x60, 0x60,
    0x30, 0x30, 0x18, 0x18, 0x18, 0xf, 0xf8,

    /* U+005B "[" */
    0xfc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc, 0xcc,
    0xcf,

    /* U+005C "\\" */
    0x40, 0x81, 0x81, 0x3, 0x6, 0x4, 0xc, 0x18,
    0x10, 0x30, 0x20, 0x60, 0xc0, 0x80,

    /* U+005D "]" */
    0xf3, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33,
    0x3f,

    /* U+005E "^" */
    0x20, 0xc7, 0x16, 0x5b, 0x28, 0xc0,

    /* U+005F "_" */
    0xff,

    /* U+0060 "`" */
    0xc6, 0x30,

    /* U+0061 "a" */
    0x3c, 0xe7, 0xc3, 0x3, 0x3f, 0xe3, 0xc3, 0xc3,
    0xe7, 0x7f,

    /* U+0062 "b" */
    0xc0, 0xc0, 0xc0, 0xc0, 0xdc, 0xe6, 0xc3, 0xc3,
    0xc3, 0xc3, 0xc3, 0xc3, 0xe6, 0xfc,

    /* U+0063 "c" */
    0x3c, 0x62, 0xc3, 0xc0, 0xc0, 0xc0, 0xc0, 0xc3,
    0x62, 0x3c,

    /* U+0064 "d" */
    0x3, 0x3, 0x3, 0x3, 0x3f, 0x67, 0xc3, 0xc3,
    0xc3, 0xc3, 0xc3, 0xc3, 0x67, 0x3f,

    /* U+0065 "e" */
    0x3c, 0x66, 0xc3, 0xc3, 0xff, 0xc0, 0xc0, 0xc0,
    0x63, 0x3e,

    /* U+0066 "f" */
    0x3d, 0x86, 0x18, 0xf9, 0x86, 0x18, 0x61, 0x86,
    0x18, 0x61, 0x80,

    /* U+0067 "g" */
    0x3f, 0x67, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0x67, 0x3f, 0x3, 0x43, 0x66, 0x3c,

    /* U+0068 "h" */
    0xc0, 0xc0, 0xc0, 0xc0, 0xde, 0xe3, 0xc3, 0xc3,
    0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,

    /* U+0069 "i" */
    0xf3, 0xff, 0xff, 0xc0,

    /* U+006A "j" */
    0x33, 0x3, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33,
    0xe0,

    /* U+006B "k" */
    0xc0, 0xc0, 0xc0, 0xc0, 0xc6, 0xce, 0xcc, 0xd8,
    0xf0, 0xf8, 0xcc, 0xc4, 0xc6, 0xc3,

    /* U+006C "l" */
    0xff, 0xff, 0xff, 0xf0,

    /* U+006D "m" */
    0xde, 0xfb, 0x9e, 0x7c, 0x30, 0xf0, 0xc3, 0xc3,
    0xf, 0xc, 0x3c, 0x30, 0xf0, 0xc3, 0xc3, 0xf,
    0xc, 0x30,

    /* U+006E "n" */
    0xde, 0xe3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xc3, 0xc3,

    /* U+006F "o" */
    0x3e, 0x31, 0xb0, 0x78, 0x3c, 0x1e, 0xf, 0x7,
    0x83, 0x63, 0x1f, 0x0,

    /* U+0070 "p" */
    0xfc, 0xe6, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xe6, 0xfc, 0xc0, 0xc0, 0xc0, 0xc0,

    /* U+0071 "q" */
    0x3f, 0x67, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0x67, 0x3f, 0x3, 0x3, 0x3, 0x3,

    /* U+0072 "r" */
    0xff, 0x31, 0x8c, 0x63, 0x18, 0xc6, 0x0,

    /* U+0073 "s" */
    0x3c, 0xc7, 0xc3, 0xe0, 0x7c, 0x3e, 0x7, 0xc3,
    0x63, 0x3c,

    /* U+0074 "t" */
    0x31, 0x8d, 0xf3, 0x18, 0xc6, 0x31, 0x8c, 0x61,
    0x80,

    /* U+0075 "u" */
    0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xe7, 0x7f,

    /* U+0076 "v" */
    0xc1, 0x43, 0x63, 0x62, 0x26, 0x36, 0x14, 0x1c,
    0x18, 0x8,

    /* U+0077 "w" */
    0x43, 0x1a, 0x18, 0xd9, 0xc4, 0xca, 0x22, 0x5b,
    0x16, 0x58, 0xf2, 0x87, 0x1c, 0x18, 0x60, 0xc2,
    0x0,

    /* U+0078 "x" */
    0x63, 0x31, 0xd, 0x83, 0x81, 0xc0, 0xe0, 0x50,
    0x6c, 0x63, 0x31, 0x80,

    /* U+0079 "y" */
    0x41, 0x31, 0x98, 0xc4, 0x43, 0x61, 0xb0, 0x50,
    0x38, 0x1c, 0x4, 0x2, 0x3, 0x1, 0x3, 0x0,

    /* U+007A "z" */
    0xfe, 0x6, 0xc, 0x18, 0x18, 0x30, 0x20, 0x60,
    0xc0, 0xff,

    /* U+007B "{" */
    0xc, 0x63, 0xc, 0x30, 0xc3, 0x18, 0xc1, 0x83,
    0xc, 0x30, 0xc3, 0x6, 0xc,

    /* U+007C "|" */
    0xff, 0xff, 0xff, 0xff,

    /* U+007D "}" */
    0xc1, 0x83, 0xc, 0x30, 0xc3, 0x4, 0xc, 0x43,
    0xc, 0x30, 0xc3, 0x18, 0xc0,

    /* U+007E "~" */
    0x70, 0xf3, 0x3c, 0x38,

    /* U+0404 "Є" */
    0x1f, 0xc, 0x66, 0xf, 0x3, 0xc0, 0x30, 0xf,
    0xe3, 0x0, 0xc0, 0x30, 0x36, 0xd, 0xc6, 0x1f,
    0x0,

    /* U+0406 "І" */
    0xff, 0xff, 0xff, 0xc0,

    /* U+0407 "Ї" */
    0xcf, 0x30, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0xc, 0x30, 0xc3, 0xc,

    /* U+0410 "А" */
    0x6, 0x0, 0x60, 0xf, 0x0, 0xb0, 0x19, 0x1,
    0x98, 0x11, 0x83, 0x8, 0x3f, 0xc2, 0x4, 0x60,
    0x64, 0x6, 0xc0, 0x20,

    /* U+0411 "Б" */
    0xff, 0x60, 0x30, 0x18, 0xc, 0x7, 0xf3, 0xd,
    0x83, 0xc1, 0xe0, 0xf0, 0x78, 0x6f, 0xe0,

    /* U+0412 "В" */
    0xfe, 0x61, 0xb0, 0x78, 0x3c, 0x1e, 0x1b, 0xf9,
    0x86, 0xc1, 0xe0, 0xf0, 0x78, 0x6f, 0xe0,

    /* U+0413 "Г" */
    0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xc0,

    /* U+0414 "Д" */
    0x1f, 0xe0, 0xc3, 0x6, 0x18, 0x30, 0xc1, 0x86,
    0xc, 0x30, 0x61, 0x83, 0xc, 0x18, 0x61, 0x83,
    0xc, 0x18, 0x40, 0xcf, 0xff, 0xe0, 0xf, 0x0,
    0x78, 0x3,

    /* U+0415 "Е" */
    0xff, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xff, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xff,

    /* U+0416 "Ж" */
    0xc1, 0x86, 0x61, 0x86, 0x31, 0x8c, 0x31, 0x8c,
    0x19, 0x98, 0x9, 0x90, 0xf, 0xf0, 0x19, 0x98,
    0x19, 0x98, 0x31, 0x8c, 0x61, 0x8e, 0x61, 0x86,
    0xc1, 0x83,

    /* U+0417 "З" */
    0x3e, 0x31, 0xb0, 0x60, 0x30, 0x18, 0x18, 0x78,
    0x6, 0x1, 0xe0, 0xf0, 0x6c, 0x63, 0xe0,

    /* U+0418 "И" */
    0xc0, 0xf0, 0x7c, 0x3f, 0xf, 0xc6, 0xf1, 0xbc,
    0xcf, 0x63, 0xd8, 0xfc, 0x3f, 0xf, 0x83, 0xc0,
    0xc0,

    /* U+0419 "Й" */
    0x33, 0x4, 0xc1, 0xe0, 0x0, 0xc0, 0xf0, 0x7c,
    0x3f, 0xf, 0xc6, 0xf1, 0xbc, 0xcf, 0x63, 0xd8,
    0xfc, 0x3f, 0xf, 0x83, 0xc0, 0xc0,

    /* U+041A "К" */
    0xc1, 0xf0, 0x6c, 0x33, 0x18, 0xcc, 0x33, 0xf,
    0x83, 0x30, 0xc6, 0x31, 0x8c, 0x33, 0x6, 0xc0,
    0xc0,

    /* U+041B "Л" */
    0x1f, 0xe3, 0xc, 0x61, 0x8c, 0x31, 0x86, 0x30,
    0xc6, 0x18, 0xc3, 0x18, 0x63, 0xc, 0x61, 0x98,
    0x3e, 0x6,

    /* U+041C "М" */
    0xe0, 0x3f, 0x1, 0xf8, 0xf, 0xe0, 0xfd, 0x5,
    0xec, 0x6f, 0x63, 0x79, 0x13, 0xcd, 0x9e, 0x28,
    0xf1, 0x47, 0x8e, 0x3c, 0x21, 0x80,

    /* U+041D "Н" */
    0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0, 0xf0, 0x3f,
    0xff, 0x3, 0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0,
    0xc0,

    /* U+041E "О" */
    0x1e, 0xc, 0xe6, 0x1b, 0x3, 0xc0, 0xf0, 0x3c,
    0xf, 0x3, 0xc0, 0xf0, 0x36, 0x19, 0xc6, 0x1e,
    0x0,

    /* U+041F "П" */
    0xff, 0xf0, 0x3c, 0xf, 0x3, 0xc0, 0xf0, 0x3c,
    0xf, 0x3, 0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0,
    0xc0,

    /* U+0420 "Р" */
    0xfe, 0x61, 0xb0, 0x78, 0x3c, 0x1e, 0xf, 0xd,
    0xfc, 0xc0, 0x60, 0x30, 0x18, 0xc, 0x0,

    /* U+0421 "С" */
    0x1f, 0xc, 0x66, 0xf, 0x3, 0xc0, 0x30, 0xc,
    0x3, 0x0, 0xc0, 0x30, 0x36, 0xd, 0xc6, 0x1f,
    0x0,

    /* U+0422 "Т" */
    0xff, 0xc3, 0x0, 0xc0, 0x30, 0xc, 0x3, 0x0,
    0xc0, 0x30, 0xc, 0x3, 0x0, 0xc0, 0x30, 0xc,
    0x0,

    /* U+0423 "У" */
    0x60, 0xcc, 0x19, 0x82, 0x18, 0xc3, 0x18, 0x36,
    0x6, 0xc0, 0x70, 0xe, 0x0, 0xc0, 0x30, 0x6,
    0x3, 0x80,

    /* U+0424 "Ф" */
    0x6, 0x0, 0x60, 0x1f, 0x86, 0x66, 0x66, 0x6c,
    0x63, 0xc6, 0x3c, 0x63, 0xc6, 0x36, 0x66, 0x66,
    0x61, 0xf8, 0x6, 0x0, 0x60,

    /* U+0425 "Х" */
    0x60, 0xc4, 0x18, 0xc6, 0xc, 0xc1, 0xb0, 0x1c,
    0x1, 0x80, 0x70, 0x1b, 0x3, 0x30, 0xc6, 0x30,
    0x66, 0xe,

    /* U+0426 "Ц" */
    0xc0, 0xd8, 0x1b, 0x3, 0x60, 0x6c, 0xd, 0x81,
    0xb0, 0x36, 0x6, 0xc0, 0xd8, 0x1b, 0x3, 0x60,
    0x6f, 0xfe, 0x0, 0xc0, 0x18, 0x3,

    /* U+0427 "Ч" */
    0xc0, 0xf0, 0x3c, 0xf, 0x3, 0xc0, 0xf0, 0x36,
    0xc, 0xff, 0x0, 0xc0, 0x30, 0xc, 0x3, 0x0,
    0xc0,

    /* U+0428 "Ш" */
    0xc3, 0xf, 0xc, 0x3c, 0x30, 0xf0, 0xc3, 0xc3,
    0xf, 0xc, 0x3c, 0x30, 0xf0, 0xc3, 0xc3, 0xf,
    0xc, 0x3c, 0x30, 0xf0, 0xc3, 0xff, 0xfc,

    /* U+0429 "Щ" */
    0xc3, 0xd, 0x86, 0x1b, 0xc, 0x36, 0x18, 0x6c,
    0x30, 0xd8, 0x61, 0xb0, 0xc3, 0x61, 0x86, 0xc3,
    0xd, 0x86, 0x1b, 0xc, 0x36, 0x18, 0x6f, 0xff,
    0xe0, 0x0, 0xc0, 0x1, 0x80, 0x3,

    /* U+042E "Ю" */
    0xc3, 0xe3, 0x18, 0xcc, 0xc1, 0xb6, 0x3, 0xd8,
    0xf, 0x60, 0x3f, 0x80, 0xf6, 0x3, 0xd8, 0xf,
    0x60, 0x3c, 0xc1, 0xb1, 0x8c, 0xc3, 0xe0,

    /* U+042F "Я" */
    0x1f, 0xcc, 0x36, 0xd, 0x83, 0x60, 0xd8, 0x33,
    0xc, 0x7f, 0x18, 0xc4, 0x33, 0xd, 0x83, 0x60,
    0xc0,

    /* U+0430 "а" */
    0x3c, 0xe7, 0xc3, 0x3, 0x3f, 0xe3, 0xc3, 0xc3,
    0xe7, 0x7f,

    /* U+0431 "б" */
    0x1, 0x1, 0x87, 0xc7, 0x6, 0x2, 0x3, 0x79,
    0xc6, 0xc1, 0xe0, 0xf0, 0x78, 0x3c, 0x1b, 0x18,
    0xf8,

    /* U+0432 "в" */
    0xfc, 0xc7, 0xc3, 0xc3, 0xc6, 0xfc, 0xc3, 0xc3,
    0xc3, 0xfe,

    /* U+0433 "г" */
    0xff, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3, 0x0,

    /* U+0434 "д" */
    0x3f, 0x8c, 0x63, 0x18, 0xc6, 0x31, 0x8c, 0x63,
    0x18, 0x86, 0x61, 0xbf, 0xfc, 0xf, 0x3, 0xc0,
    0xc0,

    /* U+0435 "е" */
    0x3c, 0x66, 0xc3, 0xc3, 0xff, 0xc0, 0xc0, 0xc0,
    0x63, 0x3e,

    /* U+0436 "ж" */
    0x63, 0x19, 0x8c, 0x63, 0x33, 0x6, 0xd8, 0xf,
    0xe0, 0x6d, 0x83, 0x33, 0xc, 0xcc, 0x63, 0x1b,
    0xc, 0x30,

    /* U+0437 "з" */
    0x7d, 0x8f, 0x18, 0x33, 0x80, 0xc1, 0xe3, 0xe6,
    0xf8,

    /* U+0438 "и" */
    0xc3, 0xc7, 0xc7, 0xcf, 0xcb, 0xd3, 0xf3, 0xe3,
    0xe3, 0xc3,

    /* U+0439 "й" */
    0x26, 0x3c, 0x0, 0xc3, 0xc7, 0xc7, 0xcf, 0xcb,
    0xd3, 0xf3, 0xe3, 0xe3, 0xc3,

    /* U+043A "к" */
    0xc3, 0x63, 0x31, 0x99, 0x8d, 0x87, 0xc3, 0x21,
    0x98, 0xc6, 0x61, 0x80,

    /* U+043B "л" */
    0x3f, 0x98, 0xcc, 0x66, 0x33, 0x19, 0x8c, 0xc6,
    0x63, 0xe1, 0xf0, 0xc0,

    /* U+043C "м" */
    0xe0, 0xfc, 0x1f, 0x83, 0xf8, 0xfd, 0x17, 0xb6,
    0xf2, 0x9e, 0x53, 0xce, 0x78, 0x8c,

    /* U+043D "н" */
    0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xff, 0xc3, 0xc3,
    0xc3, 0xc3,

    /* U+043E "о" */
    0x3e, 0x31, 0xb0, 0x78, 0x3c, 0x1e, 0xf, 0x7,
    0x83, 0x63, 0x1f, 0x0,

    /* U+043F "п" */
    0xff, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xc3, 0xc3,

    /* U+0440 "р" */
    0xfc, 0xe6, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0xc3,
    0xe6, 0xfc, 0xc0, 0xc0, 0xc0, 0xc0,

    /* U+0441 "с" */
    0x3c, 0x62, 0xc3, 0xc0, 0xc0, 0xc0, 0xc0, 0xc3,
    0x62, 0x3c,

    /* U+0442 "т" */
    0xff, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18,
    0x18, 0x18,

    /* U+0443 "у" */
    0x41, 0x31, 0x98, 0xc4, 0x43, 0x61, 0xb0, 0x50,
    0x38, 0x1c, 0x4, 0x2, 0x3, 0x1, 0x3, 0x0,

    /* U+0444 "ф" */
    0x6, 0x0, 0xc0, 0x18, 0x3, 0x3, 0xfc, 0xcd,
    0xb1, 0x9e, 0x33, 0xc6, 0x78, 0xcf, 0x19, 0xe3,
    0x36, 0x6c, 0x7f, 0x81, 0x80, 0x30, 0x6, 0x0,
    0xc0,

    /* U+0445 "х" */
    0x63, 0x31, 0xd, 0x83, 0x81, 0xc0, 0xe0, 0x50,
    0x6c, 0x63, 0x31, 0x80,

    /* U+0446 "ц" */
    0xc3, 0x61, 0xb0, 0xd8, 0x6c, 0x36, 0x1b, 0xd,
    0x86, 0xc3, 0x7f, 0xc0, 0x60, 0x30, 0x18,

    /* U+0447 "ч" */
    0xc3, 0xc3, 0xc3, 0xc3, 0xc3, 0x63, 0x3f, 0x3,
    0x3, 0x3,

    /* U+0448 "ш" */
    0xc6, 0x3c, 0x63, 0xc6, 0x3c, 0x63, 0xc6, 0x3c,
    0x63, 0xc6, 0x3c, 0x63, 0xc6, 0x3f, 0xff,

    /* U+0449 "щ" */
    0xc6, 0x36, 0x31, 0xb1, 0x8d, 0x8c, 0x6c, 0x63,
    0x63, 0x1b, 0x18, 0xd8, 0xc6, 0xc6, 0x37, 0xff,
    0xc0, 0x6, 0x0, 0x30, 0x1, 0x80,

    /* U+044C "ь" */
    0xc0, 0xc0, 0xc0, 0xc0, 0xfe, 0xc7, 0xc3, 0xc3,
    0xc7, 0xfe,

    /* U+044E "ю" */
    0xc3, 0xe6, 0x31, 0xb1, 0xf, 0x98, 0x3f, 0xc1,
    0xe6, 0xf, 0x30, 0x78, 0x83, 0xc6, 0x36, 0xf,
    0x0,

    /* U+044F "я" */
    0x1f, 0xb8, 0xd8, 0x6c, 0x36, 0x19, 0xfc, 0x46,
    0x63, 0x61, 0xb0, 0xc0,

    /* U+0454 "є" */
    0x3c, 0x62, 0xc3, 0xc0, 0xf8, 0xc0, 0xc1, 0xc3,
    0x62, 0x3c,

    /* U+0456 "і" */
    0xf3, 0xff, 0xff, 0xc0,

    /* U+0457 "ї" */
    0xcf, 0x30, 0xc, 0x30, 0xc3, 0xc, 0x30, 0xc3,
    0xc, 0x30,

    /* U+0490 "Ґ" */
    0x3, 0x3, 0x3, 0x3, 0xff, 0xc0, 0xc0, 0xc0,
    0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0, 0xc0,
    0xc0,

    /* U+0491 "ґ" */
    0xc, 0x30, 0xff, 0xc3, 0xc, 0x30, 0xc3, 0xc,
    0x30, 0xc0
};


/*---------------------
 *  GLYPH DESCRIPTION
 *--------------------*/

static const lv_font_fmt_txt_glyph_dsc_t glyph_dsc[] = {
    {.bitmap_index = 0, .adv_w = 0, .box_w = 0, .box_h = 0, .ofs_x = 0, .ofs_y = 0} /* id = 0 reserved */,
    {.bitmap_index = 0, .adv_w = 71, .box_w = 1, .box_h = 1, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1, .adv_w = 74, .box_w = 2, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 5, .adv_w = 92, .box_w = 3, .box_h = 4, .ofs_x = 1, .ofs_y = 10},
    {.bitmap_index = 7, .adv_w = 177, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 24, .adv_w = 162, .box_w = 8, .box_h = 17, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 41, .adv_w = 211, .box_w = 11, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 59, .adv_w = 179, .box_w = 11, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 77, .adv_w = 50, .box_w = 1, .box_h = 4, .ofs_x = 1, .ofs_y = 10},
    {.bitmap_index = 78, .adv_w = 98, .box_w = 5, .box_h = 19, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 90, .adv_w = 100, .box_w = 5, .box_h = 19, .ofs_x = 0, .ofs_y = -4},
    {.bitmap_index = 102, .adv_w = 124, .box_w = 8, .box_h = 8, .ofs_x = 0, .ofs_y = 5},
    {.bitmap_index = 110, .adv_w = 163, .box_w = 9, .box_h = 9, .ofs_x = 1, .ofs_y = 1},
    {.bitmap_index = 121, .adv_w = 57, .box_w = 3, .box_h = 5, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 123, .adv_w = 79, .box_w = 4, .box_h = 1, .ofs_x = 1, .ofs_y = 5},
    {.bitmap_index = 124, .adv_w = 76, .box_w = 2, .box_h = 2, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 125, .adv_w = 119, .box_w = 7, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 139, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 152, .adv_w = 162, .box_w = 6, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 162, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 175, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 188, .adv_w = 162, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 203, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 216, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 229, .adv_w = 162, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 244, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 257, .adv_w = 162, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 270, .adv_w = 70, .box_w = 2, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 273, .adv_w = 61, .box_w = 3, .box_h = 13, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 278, .adv_w = 146, .box_w = 7, .box_h = 8, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 285, .adv_w = 158, .box_w = 7, .box_h = 5, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 290, .adv_w = 150, .box_w = 8, .box_h = 8, .ofs_x = 1, .ofs_y = 2},
    {.bitmap_index = 298, .adv_w = 136, .box_w = 7, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 310, .adv_w = 259, .box_w = 14, .box_h = 17, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 340, .adv_w = 188, .box_w = 12, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 360, .adv_w = 179, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 375, .adv_w = 187, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 392, .adv_w = 189, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 407, .adv_w = 164, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 420, .adv_w = 159, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 433, .adv_w = 196, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 450, .adv_w = 205, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 467, .adv_w = 78, .box_w = 2, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 471, .adv_w = 159, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 484, .adv_w = 181, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 501, .adv_w = 155, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 514, .adv_w = 251, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 536, .adv_w = 205, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 553, .adv_w = 198, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 570, .adv_w = 182, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 585, .adv_w = 198, .box_w = 10, .box_h = 15, .ofs_x = 1, .ofs_y = -2},
    {.bitmap_index = 604, .adv_w = 177, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 621, .adv_w = 171, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 636, .adv_w = 172, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 653, .adv_w = 187, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 668, .adv_w = 183, .box_w = 11, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 686, .adv_w = 256, .box_w = 15, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 711, .adv_w = 181, .box_w = 11, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 729, .adv_w = 173, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 746, .adv_w = 172, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 761, .adv_w = 76, .box_w = 4, .box_h = 18, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 770, .adv_w = 118, .box_w = 7, .box_h = 15, .ofs_x = 0, .ofs_y = -2},
    {.bitmap_index = 784, .adv_w = 76, .box_w = 4, .box_h = 18, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 793, .adv_w = 120, .box_w = 6, .box_h = 7, .ofs_x = 1, .ofs_y = 7},
    {.bitmap_index = 799, .adv_w = 130, .box_w = 8, .box_h = 1, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 800, .adv_w = 89, .box_w = 4, .box_h = 3, .ofs_x = 0, .ofs_y = 11},
    {.bitmap_index = 802, .adv_w = 157, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 812, .adv_w = 162, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 826, .adv_w = 151, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 836, .adv_w = 162, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 850, .adv_w = 153, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 860, .adv_w = 100, .box_w = 6, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 871, .adv_w = 162, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 885, .adv_w = 159, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 899, .adv_w = 70, .box_w = 2, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 903, .adv_w = 69, .box_w = 4, .box_h = 17, .ofs_x = -1, .ofs_y = -4},
    {.bitmap_index = 912, .adv_w = 146, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 926, .adv_w = 70, .box_w = 2, .box_h = 14, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 930, .adv_w = 252, .box_w = 14, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 948, .adv_w = 159, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 958, .adv_w = 164, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 970, .adv_w = 162, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 984, .adv_w = 164, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 998, .adv_w = 97, .box_w = 5, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1005, .adv_w = 149, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1015, .adv_w = 94, .box_w = 5, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1024, .adv_w = 159, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1034, .adv_w = 140, .box_w = 8, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1044, .adv_w = 216, .box_w = 13, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1061, .adv_w = 143, .box_w = 9, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1073, .adv_w = 136, .box_w = 9, .box_h = 14, .ofs_x = 0, .ofs_y = -4},
    {.bitmap_index = 1089, .adv_w = 143, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1099, .adv_w = 97, .box_w = 6, .box_h = 17, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 1112, .adv_w = 70, .box_w = 2, .box_h = 16, .ofs_x = 2, .ofs_y = -3},
    {.bitmap_index = 1116, .adv_w = 97, .box_w = 6, .box_h = 17, .ofs_x = 0, .ofs_y = -3},
    {.bitmap_index = 1129, .adv_w = 196, .box_w = 10, .box_h = 3, .ofs_x = 1, .ofs_y = 4},
    {.bitmap_index = 1133, .adv_w = 194, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1150, .adv_w = 78, .box_w = 2, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1154, .adv_w = 78, .box_w = 6, .box_h = 16, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1166, .adv_w = 188, .box_w = 12, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1186, .adv_w = 182, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1201, .adv_w = 179, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1216, .adv_w = 160, .box_w = 8, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1229, .adv_w = 217, .box_w = 13, .box_h = 16, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 1255, .adv_w = 164, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1268, .adv_w = 261, .box_w = 16, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1294, .adv_w = 171, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1309, .adv_w = 205, .box_w = 10, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1326, .adv_w = 205, .box_w = 10, .box_h = 17, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1348, .adv_w = 185, .box_w = 10, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1365, .adv_w = 204, .box_w = 11, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1383, .adv_w = 251, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1405, .adv_w = 205, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1422, .adv_w = 198, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1439, .adv_w = 205, .box_w = 10, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1456, .adv_w = 182, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1471, .adv_w = 187, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1488, .adv_w = 172, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1505, .adv_w = 181, .box_w = 11, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1523, .adv_w = 222, .box_w = 12, .box_h = 14, .ofs_x = 1, .ofs_y = -1},
    {.bitmap_index = 1544, .adv_w = 181, .box_w = 11, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1562, .adv_w = 211, .box_w = 11, .box_h = 16, .ofs_x = 2, .ofs_y = -3},
    {.bitmap_index = 1584, .adv_w = 197, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1601, .adv_w = 271, .box_w = 14, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1624, .adv_w = 279, .box_w = 15, .box_h = 16, .ofs_x = 2, .ofs_y = -3},
    {.bitmap_index = 1654, .adv_w = 257, .box_w = 14, .box_h = 13, .ofs_x = 2, .ofs_y = 0},
    {.bitmap_index = 1677, .adv_w = 183, .box_w = 10, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1694, .adv_w = 157, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1704, .adv_w = 159, .box_w = 9, .box_h = 15, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1721, .adv_w = 165, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1731, .adv_w = 121, .box_w = 6, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1739, .adv_w = 174, .box_w = 10, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 1756, .adv_w = 153, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1766, .adv_w = 221, .box_w = 14, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1784, .adv_w = 146, .box_w = 7, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1793, .adv_w = 166, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1803, .adv_w = 166, .box_w = 8, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1816, .adv_w = 156, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1828, .adv_w = 167, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1840, .adv_w = 214, .box_w = 11, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1854, .adv_w = 166, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1864, .adv_w = 164, .box_w = 9, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1876, .adv_w = 166, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1886, .adv_w = 162, .box_w = 8, .box_h = 14, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 1900, .adv_w = 151, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1910, .adv_w = 138, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1920, .adv_w = 136, .box_w = 9, .box_h = 14, .ofs_x = 0, .ofs_y = -4},
    {.bitmap_index = 1936, .adv_w = 209, .box_w = 11, .box_h = 18, .ofs_x = 1, .ofs_y = -4},
    {.bitmap_index = 1961, .adv_w = 143, .box_w = 9, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 1973, .adv_w = 171, .box_w = 9, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 1988, .adv_w = 157, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 1998, .adv_w = 233, .box_w = 12, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2013, .adv_w = 238, .box_w = 13, .box_h = 13, .ofs_x = 1, .ofs_y = -3},
    {.bitmap_index = 2035, .adv_w = 156, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2045, .adv_w = 235, .box_w = 13, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2062, .adv_w = 158, .box_w = 9, .box_h = 10, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2074, .adv_w = 155, .box_w = 8, .box_h = 10, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2084, .adv_w = 70, .box_w = 2, .box_h = 13, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2088, .adv_w = 71, .box_w = 6, .box_h = 13, .ofs_x = 0, .ofs_y = 0},
    {.bitmap_index = 2098, .adv_w = 158, .box_w = 8, .box_h = 17, .ofs_x = 1, .ofs_y = 0},
    {.bitmap_index = 2115, .adv_w = 128, .box_w = 6, .box_h = 13, .ofs_x = 1, .ofs_y = 0}
};

/*---------------------
 *  CHARACTER MAPPING
 *--------------------*/

static const uint8_t glyph_id_ofs_list_1[] = {
    0, 0, 1, 2
};

static const uint16_t unicode_list_4[] = {
    0x0, 0x2, 0x3, 0x8, 0xa, 0xb, 0x44, 0x45
};

/*Collect the unicode lists and glyph_id offsets*/
static const lv_font_fmt_txt_cmap_t cmaps[] =
{
    {
        .range_start = 32, .range_length = 95, .glyph_id_start = 1,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 1028, .range_length = 4, .glyph_id_start = 96,
        .unicode_list = NULL, .glyph_id_ofs_list = glyph_id_ofs_list_1, .list_length = 4, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_FULL
    },
    {
        .range_start = 1040, .range_length = 26, .glyph_id_start = 99,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 1070, .range_length = 28, .glyph_id_start = 125,
        .unicode_list = NULL, .glyph_id_ofs_list = NULL, .list_length = 0, .type = LV_FONT_FMT_TXT_CMAP_FORMAT0_TINY
    },
    {
        .range_start = 1100, .range_length = 70, .glyph_id_start = 153,
        .unicode_list = unicode_list_4, .glyph_id_ofs_list = NULL, .list_length = 8, .type = LV_FONT_FMT_TXT_CMAP_SPARSE_TINY
    }
};

/*-----------------
 *    KERNING
 *----------------*/


/*Map glyph_ids to kern left classes*/
static const uint8_t kern_left_class_mapping[] =
{
    0, 1, 0, 2, 0, 0, 0, 0,
    2, 3, 0, 0, 0, 4, 0, 4,
    5, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 6, 7, 8, 9, 10, 11,
    0, 12, 12, 13, 14, 15, 12, 12,
    9, 16, 17, 18, 0, 19, 13, 20,
    21, 22, 23, 24, 25, 0, 0, 0,
    0, 0, 26, 27, 28, 0, 29, 30,
    0, 31, 0, 0, 32, 0, 31, 31,
    33, 27, 0, 34, 0, 35, 0, 36,
    37, 38, 36, 39, 40, 0, 0, 0,
    0, 12, 12, 6, 41, 7, 42, 43,
    10, 22, 44, 12, 0, 14, 12, 12,
    12, 9, 0, 16, 8, 19, 45, 0,
    22, 12, 12, 12, 46, 47, 48, 26,
    49, 50, 51, 52, 29, 38, 28, 0,
    0, 53, 0, 0, 0, 33, 0, 27,
    28, 54, 36, 27, 38, 55, 0, 0,
    56, 57, 58, 0, 59, 0, 0, 42,
    51
};

/*Map glyph_ids to kern right classes*/
static const uint8_t kern_right_class_mapping[] =
{
    0, 1, 0, 2, 0, 0, 0, 3,
    2, 0, 4, 5, 0, 6, 7, 6,
    8, 0, 0, 0, 0, 0, 0, 0,
    9, 0, 0, 0, 0, 0, 0, 0,
    10, 0, 11, 0, 12, 0, 0, 0,
    12, 0, 0, 13, 0, 0, 0, 0,
    12, 0, 12, 0, 14, 15, 16, 17,
    18, 19, 20, 21, 0, 0, 22, 0,
    0, 0, 23, 0, 24, 24, 24, 25,
    24, 0, 0, 0, 0, 0, 26, 26,
    27, 26, 24, 28, 29, 30, 31, 32,
    33, 34, 32, 35, 0, 0, 36, 0,
    12, 0, 0, 11, 0, 0, 0, 37,
    0, 19, 0, 0, 0, 0, 38, 0,
    0, 12, 0, 0, 12, 15, 39, 0,
    19, 0, 40, 0, 0, 0, 0, 23,
    41, 42, 26, 43, 24, 34, 44, 26,
    26, 26, 45, 26, 26, 27, 26, 26,
    24, 46, 32, 24, 34, 26, 47, 26,
    26, 26, 26, 48, 24, 0, 0, 0,
    49
};

/*Kern values between classes*/
static const int8_t kern_class_values[] =
{
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -6, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -15, 0, 0, 0, 0, 0,
    0, 0, 0, -17, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -7,
    -8, 0, -3, -9, 0, -11, 0, 0,
    0, 2, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 3, 3, 0, 3, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -24, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -31, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -17, 0,
    0, 0, 0, 0, 0, 0, -9, 0,
    -2, 0, 0, -18, -2, -12, -10, 0,
    -13, 0, 0, 0, 0, 0, 0, -2,
    0, 0, -2, -2, -7, -5, 0, 2,
    0, 3, 2, 0, -8, 0, 0, 0,
    0, 3, -8, -16, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -4, 0, -3, 0,
    0, -8, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -2, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -4, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -4, 0, 0,
    0, 0, 0, 0, -2, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -2, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -14, 0, 0,
    0, 0, -3, 0, 0, 0, -4, 0,
    -3, 0, -3, -6, -3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -5, -4, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 3,
    0, 0, 0, 0, 0, 0, 0, 0,
    -3, -3, 0, -3, 0, 0, 0, -2,
    -4, -3, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -33,
    0, 0, 0, 0, -24, 0, -37, 0,
    3, 0, 0, 0, 0, 0, 0, 0,
    -5, -3, 0, 0, -3, -4, 0, 0,
    -3, -3, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 3, 0, 0,
    0, -4, 0, 0, 0, 2, -4, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 3,
    2, 0, -4, 0, 0, 3, 0, 2,
    0, -4, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -3, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -9, 0, 0, 0, 0,
    -4, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -4, 0, -3, -4,
    0, 0, 0, -3, -6, -9, 0, 0,
    0, 0, 0, 0, 0, -6, 0, 0,
    0, 0, -10, -13, 0, 0, 0, -47,
    0, 0, 0, 0, 0, 0, 0, 0,
    3, -9, 0, 0, -39, -8, -25, -20,
    0, -34, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -6, -19, -13, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -46, 0, 0, 0,
    0, -19, 0, -28, 0, 0, 0, 0,
    0, -4, 0, -4, 0, -2, -2, 0,
    0, -2, 0, 0, 2, 0, 2, 0,
    0, 0, 0, -12, -8, 0, 0, 0,
    0, -8, 0, -5, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -6, 0,
    -4, -3, 0, -5, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -11,
    0, -3, 0, 0, -7, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -6, 0, 0, 0, 0, -31,
    -33, 0, 0, 0, -11, -4, -34, -2,
    2, 0, 2, 2, 0, 2, 0, 0,
    -16, -14, 0, -15, -14, -11, -16, 0,
    -13, -10, -8, -11, -8, 0, -13, -7,
    0, 0, -4, -13, -20, -18, -18, -11,
    -19, -19, -15, 0, 0, 0, 3, 0,
    -32, -5, 0, 0, 0, -11, -2, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    2, -6, -6, 0, 0, -6, -4, 0,
    0, -4, -2, 0, 0, 0, 3, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 2,
    0, -17, -8, 0, 0, 0, -6, 0,
    0, 0, 2, 0, 0, 0, 0, 0,
    0, 2, -5, -4, 0, 0, -4, -3,
    0, 0, -3, 0, 0, 0, 0, 2,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -6, 0, 0, 0, 0,
    -4, 0, 0, 0, 0, 2, 0, 0,
    0, 0, 0, 0, -4, 0, 0, -3,
    0, 0, 0, -3, -4, 0, 0, 0,
    0, 3, 2, 0, 0, -3, 0, 0,
    0, 2, -6, -7, 0, 0, 0, 0,
    -4, 3, -7, -30, -7, 0, 0, 0,
    -13, -4, -13, -2, 2, -13, 3, 2,
    2, 3, 0, 3, -10, -9, -3, -6,
    -9, -6, -8, -3, -5, -3, 0, -3,
    -4, 3, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 2, -4, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -3, 0,
    0, -3, 0, 0, 0, -3, -4, -4,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -3, 0, 0, -3,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -9, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    -2, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -2, 0,
    0, 0, 0, -4, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -2, 0, -2, -2, 0, 0, 0,
    0, 0, 0, 0, -3, 0, -2, -11,
    0, 0, 0, 0, -2, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -2, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -2, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -2, 0, 0, 0, 0, 2, 0,
    3, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 3, 0, -3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    3, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -15,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -9, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -3, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -19, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -2,
    0, -3, -2, 0, 0, 0, 0, 0,
    0, 0, -10, 0, -2, -3, 0, 0,
    0, 0, 2, 0, 0, 0, -17, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -6,
    -3, 2, 0, -3, 0, 0, 7, 0,
    3, 2, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 2, 0, 0, 0,
    -15, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -2, -2, 2, 0, -2, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -8, 0, -6,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -17, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -3, 0, 0, -3,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -2, 0, 0,
    -2, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -3, 0, 0, -3, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -4, 0,
    -4, 0, -2, -4, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -2, 0, 0, 0, 0, -3, -3,
    0, 0, 0, 0, 0, -7, 0, 0,
    0, 0, 0, 0, 0, 0, -55, -58,
    0, 0, 0, -27, -9, 0, -7, 1,
    0, 1, 1, 0, 1, 0, 0, -30,
    -26, 0, -29, -26, -21, -31, 0, -25,
    -20, -16, -21, -17, 0, -24, -15, 0,
    0, -9, -26, -36, -33, -34, -21, -34,
    -35, -28, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 2, -3, 0, 0,
    -4, 0, -4, 0, 0, -5, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 3, 2,
    0, -4, 0, 0, 3, 0, 2, 0,
    -4, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -2, 0, 0, 0, 0,
    0, -2, 0, -2, 0, -2, -2, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    -2, -2, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -57, -17, 0, 0, 0, -13, -3,
    0, 0, 3, 0, 0, 0, 0, 3,
    0, 0, -16, -9, 0, -14, -9, 0,
    -6, 0, 0, 0, 0, 0, 0, 0,
    -12, -8, 0, 0, 0, -4, -12, -6,
    -10, 0, -3, -6, -4, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 2,
    0, 0, 0, -4, 0, -4, 0, 2,
    -4, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 3, 2, 2, -4, 0, 0, 3,
    0, 2, -4, -3, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -4, 0, 0, 0,
    -4, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, -4, -4, -3, 0, 0, 0,
    -4, 0, -3, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -3, 0, 0,
    0, 0, -2, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -2, 0, 0, 0, 0, 0, 0,
    0, -2, 0, 0, 0, 0, 0, 0,
    0, 0, -3, 0, 0, -3, 0, 0,
    0, 0, -2, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    -2, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, -2, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, -3, 0, 0, -3, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, -8, 0, -6, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 3, 0, 0,
    -3, -3, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -2, 0, 0, -3, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 2, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, -2, -11, 2, 0, -6,
    0, 0, 0, 0, 2, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -12,
    0, -11, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    3, 0, 2, -2, -2, 2, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 2, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 8, 0, 3, -4, -1, 0, 0,
    0, -19, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, -5,
    0, -2, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, -10, -5, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    -2, 0, -2, 0, 0, 0, 0, 0,
    0, 0, 0, -2, 0, -2, 0, 0,
    0, 0, 0, -6, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0
};


/*Collect the kern class' data in one place*/
static const lv_font_fmt_txt_kern_classes_t kern_classes =
{
    .class_pair_values   = kern_class_values,
    .left_class_mapping  = kern_left_class_mapping,
    .right_class_mapping = kern_right_class_mapping,
    .left_class_cnt      = 59,
    .right_class_cnt     = 49,
};

/*--------------------
 *  ALL CUSTOM DATA
 *--------------------*/

#if LV_VERSION_CHECK(8, 0, 0)
/*Store all the custom data of the font*/
static  lv_font_fmt_txt_glyph_cache_t cache;
static const lv_font_fmt_txt_dsc_t font_dsc = {
#else
static lv_font_fmt_txt_dsc_t font_dsc = {
#endif
    .glyph_bitmap = glyph_bitmap,
    .glyph_dsc = glyph_dsc,
    .cmaps = cmaps,
    .kern_dsc = &kern_classes,
    .kern_scale = 16,
    .cmap_num = 5,
    .bpp = 1,
    .kern_classes = 1,
    .bitmap_format = 0,
#if LV_VERSION_CHECK(8, 0, 0)
    .cache = &cache
#endif
};


/*-----------------
 *  PUBLIC FONT
 *----------------*/

/*Initialize a public general font descriptor*/
#if LV_VERSION_CHECK(8, 0, 0)
const lv_font_t ui_font_RobotoBold16 = {
#else
lv_font_t ui_font_RobotoBold16 = {
#endif
    .get_glyph_dsc = lv_font_get_glyph_dsc_fmt_txt,    /*Function pointer to get glyph's data*/
    .get_glyph_bitmap = lv_font_get_bitmap_fmt_txt,    /*Function pointer to get glyph's bitmap*/
    .line_height = 21,          /*The maximum line height required by the font*/
    .base_line = 4,             /*Baseline measured from the bottom of the line*/
#if !(LVGL_VERSION_MAJOR == 6 && LVGL_VERSION_MINOR == 0)
    .subpx = LV_FONT_SUBPX_NONE,
#endif
#if LV_VERSION_CHECK(7, 4, 0) || LVGL_VERSION_MAJOR >= 8
    .underline_position = -1,
    .underline_thickness = 1,
#endif
    .dsc = &font_dsc           /*The custom font data. Will be accessed by `get_glyph_bitmap/dsc` */
};



#endif /*#if UI_FONT_ROBOTOBOLD16*/

