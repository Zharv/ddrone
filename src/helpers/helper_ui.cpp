#include "store/store.h"
#include "ui/ui.h"

static void onBattetySwitchChanged(lv_event_t *e)
{
    if (lv_obj_has_state(e->target, LV_STATE_CHECKED) == true)
    {
        digitalWrite(BMS_SWITH_PIN, HIGH);
        lv_obj_clear_flag(ui_BatteryInfoPanel, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_state(ui_InvertorSwich, LV_STATE_DISABLED);
        lv_obj_clear_flag(ui_BatteryCurrentLabel, LV_OBJ_FLAG_HIDDEN);
        lv_obj_clear_flag(ui_BatteryVoltageLabel, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        digitalWrite(BMS_SWITH_PIN, LOW);
        digitalWrite(INV_SWICH_PIN, LOW);
        lv_obj_add_flag(ui_BatteryInfoPanel, LV_OBJ_FLAG_HIDDEN);
        lv_obj_add_flag(ui_InvertorInfoPanel, LV_OBJ_FLAG_HIDDEN);
        lv_obj_add_flag(ui_BatteryCurrentLabel, LV_OBJ_FLAG_HIDDEN);
        lv_obj_add_flag(ui_BatteryVoltageLabel, LV_OBJ_FLAG_HIDDEN);
        
        lv_obj_clear_state(ui_InvertorSwich, LV_STATE_CHECKED);
        lv_obj_add_state(ui_InvertorSwich, LV_STATE_DISABLED);
    }
}

static void onInverterSwitchChanged(lv_event_t *e)
{
    if (lv_obj_has_state(e->target, LV_STATE_CHECKED) == true)
    {
        digitalWrite(INV_SWICH_PIN, HIGH);
        lv_obj_clear_flag(ui_InvertorInfoPanel, LV_OBJ_FLAG_HIDDEN);
    }
    else
    {
        digitalWrite(INV_SWICH_PIN, LOW);
        lv_obj_add_flag(ui_InvertorInfoPanel, LV_OBJ_FLAG_HIDDEN);
    }
}