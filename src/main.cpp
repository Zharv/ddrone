#define LGFX_USE_V1 // set to use new version of library
#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE
#define BUFFER_SIZE 3
#define DRONE_THRESHOLD 0.75

#include <deque>
#include <Wire.h>
#include <WiFi.h>
#include <SD.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "driver/uart.h"
#include "devices/conf_WT32SCO1-Plus.h"

#include "config.h"
#include "esp_log.h"

#include "usb_stream.h"
#include <Zharv-project-1_inferencing.h>
#include <edge-impulse-sdk/dsp/spectral/filters.hpp>

static const char *TAG = "Main";

/** Audio buffers, pointers and selectors */
typedef struct
{
  int16_t *buffer;
  uint8_t buf_ready;
  uint32_t buf_count;
  uint32_t n_samples;
} inference_t;

static inference_t inference;
static const uint32_t sample_buffer_size = 2048;
static signed short sampleBuffer[sample_buffer_size];
static bool debug_nn = false; // Set this to true to see e.g. features generated from the raw signal
static bool record_status = true;

static LGFX tft; // declare display variable

const char *ssid = "Zharv";           // Your WiFi SSID
const char *password = "591566typok"; // Your WiFi Password

std::deque<float> drone_coefficients;

static bool microphone_inference_start(uint32_t n_samples);
static bool microphone_inference_record(void);
static void audio_inference_callback(uint32_t n_bytes);
static void capture_samples(void *buffer, size_t buffer_size_in_bytes);
static int microphone_audio_signal_get_data(size_t offset, size_t length, float *out_ptr);

void downsample_butterworth(int16_t *input_buffer, int16_t *output_buffer, size_t input_size, size_t output_size, float input_sampling_freq, float output_sampling_freq)
{
  int filter_order = 1;                                                // Задайте порядок фильтра (четное число от 2 до 8)
  float cutoff_freq = output_sampling_freq / 2.0f;                     // Найдите частоту среза для фильтрации нижних частот
  float decimation_ratio = input_sampling_freq / output_sampling_freq; // Вычислите коэффициент понижающей дискретизации

  float *input_buffer_float = new float[input_size];
  float *output_buffer_float = new float[output_size];

  // Преобразование входного буфера в массив float
  for (size_t i = 0; i < input_size; i++)
  {
    input_buffer_float[i] = static_cast<float>(input_buffer[i]);
  }

  // Примените фильтр Баттеруорта нижних частот
  ei::spectral::filters::butterworth_lowpass(filter_order, input_sampling_freq, cutoff_freq, input_buffer_float, input_buffer_float, input_size);

  // Примените понижение дискретизации
  for (size_t i = 0; i < output_size; i++)
  {
    float sum = 0;
    for (size_t k = 0; k < decimation_ratio; k++)
    {
      size_t input_idx = i * decimation_ratio + k;
      if (input_idx < input_size)
      {
        sum += input_buffer_float[input_idx];
      }
    }
    output_buffer_float[i] = sum / decimation_ratio;
  }

  // Преобразование выходного буфера float обратно в int16_t
  for (size_t i = 0; i < output_size; i++)
  {
    output_buffer[i] = std::min(std::max(-32768, static_cast<int>(output_buffer_float[i])), 32767);
  }

  delete[] input_buffer_float;
  delete[] output_buffer_float;
}

void update_display(float avg_drone_coefficient)
{
  // Линейная интерполяция между зеленым (0, 255, 0) и красным (255, 0, 0) цветами
  uint8_t red = (uint8_t)(avg_drone_coefficient * 255);
  uint8_t green = (uint8_t)((1 - avg_drone_coefficient) * 255);
  uint32_t bgColor = (red << 16) | (green << 8);

  tft.fillRect(0, 0, tft.width(), tft.height(), bgColor); // Заполните фоновый цвет

  tft.setTextSize(4);          // Установите размер текста
  tft.setTextColor(TFT_WHITE); // Установите цвет текста

  // Выведите процентное значение по центру экрана
  char percentage[10];
  snprintf(percentage, sizeof(percentage), "%.0f%%", avg_drone_coefficient * 100);
  int16_t x = (tft.width() - tft.textWidth(percentage)) / 2;
  int16_t y = (tft.height() - tft.fontHeight()) / 2;
  tft.drawString(percentage, x, y);
}

void amplify_signal(int16_t *buffer, size_t buffer_size, float gain)
{
  for (size_t i = 0; i < buffer_size; i++)
  {
    // Умножаем каждый отсчет на коэффициент усиления
    float amplified_sample = static_cast<float>(buffer[i]) * gain;

    // Ограничиваем значения в пределах допустимого диапазона для int16_t
    buffer[i] = static_cast<int16_t>(std::min(std::max(-32768.0f, amplified_sample), 32767.0f));
  }
}

static void mic_frame_cb(mic_frame_t *frame, void *ptr)
{
  // MicroPrintf("New mic frame: Bit resolution %d , samples_frequence %d, bytes %d", frame->bit_resolution, frame->samples_frequence, frame->data_bytes);

  float gain = 1.5f; // Усиление в 2 раза
  amplify_signal((int16_t *)frame->data, frame->data_bytes / 2, gain);

  float source_sampling_freq = frame->samples_frequence;              // Исходная частота дискретизации
  float target_sampling_freq = frame->samples_frequence/2;            // Целевая частота дискретизации
  int decimation_ratio = source_sampling_freq / target_sampling_freq; // 44100 / 16000 = 2.75625

  decimation_ratio = ceil(decimation_ratio);
  size_t downsampled_buffer_size = frame->data_bytes / (2 * decimation_ratio);
  // MicroPrintf("Downsample: From %d Hz to %d Hz, decimation ratio %d, buffer size %d", frame->bit_resolution, inference.n_samples, decimation_ratio, downsampled_buffer_size);

  int16_t *downsampled_buffer = new int16_t[downsampled_buffer_size];

  // Примените понижающую дискретизацию и фильтрацию нижних частот к данным кадра
  downsample_butterworth((int16_t *)frame->data, downsampled_buffer, frame->data_bytes / 2, downsampled_buffer_size, source_sampling_freq, target_sampling_freq);
  capture_samples((void *)downsampled_buffer, downsampled_buffer_size * sizeof(int16_t));

  // Очистка выделенной памяти
  delete[] downsampled_buffer;
}

void update_drone_coefficients(float drone_coefficient)
{
  if (drone_coefficients.size() >= BUFFER_SIZE)
  {
    drone_coefficients.pop_front();
  }

  drone_coefficients.push_back(drone_coefficient);

  float sum = 0;
  for (float coef : drone_coefficients)
  {
    sum += coef;
  }

  float avg = sum / drone_coefficients.size();

  // Обновление дисплея с новым цветом фона и процентным значением
    update_display(avg);


  if (avg > DRONE_THRESHOLD)
  {
    // Звук дрона обнаружен
    ei_printf("Drone detected (average: %f)\n", avg);
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  esp_log_level_set("*", ESP_LOG_ERROR);

  // Инициализация дисплея
  tft.init();
  tft.setRotation(1);
  tft.setColorDepth(16);
  tft.setBrightness(128);
  tft.fillScreen(TFT_BLACK);
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.setTextSize(1);
  tft.setTextWrap(true);
  tft.setCursor(0, 0);

  Serial.println("Edge Impulse Inferencing Demo");

  // summary of inferencing settings (from model_metadata.h)
  ei_printf("Inferencing settings:\n");
  ei_printf("\tInterval: ");
  ei_printf_float((float)EI_CLASSIFIER_INTERVAL_MS);
  ei_printf(" ms.\n");
  ei_printf("\tFrame size: %d\n", EI_CLASSIFIER_DSP_INPUT_FRAME_SIZE);
  ei_printf("\tSample length: %d ms.\n", EI_CLASSIFIER_RAW_SAMPLE_COUNT / 16);
  ei_printf("\tNo. of classes: %d\n", sizeof(ei_classifier_inferencing_categories) / sizeof(ei_classifier_inferencing_categories[0]));

  if (microphone_inference_start(EI_CLASSIFIER_RAW_SAMPLE_COUNT) == false)
  {
    ei_printf("ERR: Could not allocate audio buffer (size %d), this could be due to the window length of your model\r\n", EI_CLASSIFIER_RAW_SAMPLE_COUNT);
    return;
  }

  ei_printf("Recording...\n");
}

// Process audio data and run inference
void loop()
{
  bool m = microphone_inference_record();
  if (!m)
  {
    ei_printf("ERR: Failed to record audio...\n");
    return;
  }

  signal_t signal;
  signal.total_length = EI_CLASSIFIER_RAW_SAMPLE_COUNT;
  signal.get_data = &microphone_audio_signal_get_data;
  ei_impulse_result_t result = {0};

  EI_IMPULSE_ERROR r = run_classifier(&signal, &result, debug_nn);
  if (r != EI_IMPULSE_OK)
  {
    ei_printf("ERR: Failed to run classifier (%d)\n", r);
    return;
  }

  // print the predictions
  ei_printf("Predictions ");
  ei_printf("(DSP: %d ms., Classification: %d ms., Anomaly: %d ms.)",
            result.timing.dsp, result.timing.classification, result.timing.anomaly);
  ei_printf(": \n");
  for (size_t ix = 0; ix < EI_CLASSIFIER_LABEL_COUNT; ix++)
  {
    ei_printf("    %s: ", result.classification[ix].label);
    ei_printf_float(result.classification[ix].value);
    ei_printf("\n");
  }

  float drone_coefficient = result.classification[0].value; // Индекс 0 для метки DRONE
  update_drone_coefficients(drone_coefficient);
#if EI_CLASSIFIER_HAS_ANOMALY == 1
  ei_printf("    anomaly score: ");
  ei_printf_float(result.anomaly);
  ei_printf("\n");
#endif
}

static bool microphone_inference_start(uint32_t n_samples)
{
  inference.buffer = (int16_t *)malloc(n_samples * sizeof(int16_t));

  if (inference.buffer == NULL)
  {
    return false;
  }

  inference.buf_count = 0;
  inference.n_samples = n_samples;
  inference.buf_ready = 0;

  //.dma_buf_count = 8,
  //.dma_buf_len = 512,

  uac_config_t uac_config = {
      .mic_bit_resolution = 16,
      .mic_samples_frequence = 44100,
      .mic_cb = &mic_frame_cb,
      .mic_cb_arg = NULL,
  };

  esp_err_t ret = ESP_FAIL;

  ret = uac_streaming_config(&uac_config);
  if (ret != ESP_OK)
  {
    ei_printf("Failed to start usb!");
  }

  ret = usb_streaming_start();
  if (ret != ESP_OK)
  {
    ei_printf("Failed to start usb!");
  }

  ei_sleep(100);

  record_status = true;

  return true;
}

static void audio_inference_callback(uint32_t n_bytes)
{
  for (int i = 0; i < n_bytes >> 1; i++)
  {
    inference.buffer[inference.buf_count++] = sampleBuffer[i];

    if (inference.buf_count >= inference.n_samples)
    {
      inference.buf_count = 0;
      inference.buf_ready = 1;
    }
  }
}

static void capture_samples(void *buffer, size_t buffer_size_in_bytes)
{
  // Проверка на переполнение sampleBuffer
  if (buffer_size_in_bytes > sample_buffer_size)
  {
    ei_printf("Error: sampleBuffer overflow, read size: %zu, buffer size: %u", buffer_size_in_bytes, sample_buffer_size);
    buffer_size_in_bytes = sample_buffer_size;
  }

  // ei_printf("Bytes Available: %zu ", buffer_size_in_bytes);

  // Копирование данных из downsampled_buffer в sampleBuffer
  int16_t *input_buffer = (int16_t *)buffer;
  for (size_t i = 0; i < buffer_size_in_bytes / 2; i++)
  {
    sampleBuffer[i] = input_buffer[i];
  }

  // Вызов функции обратного вызова аудио-инференции
  audio_inference_callback(buffer_size_in_bytes);
}

/**
 * @brief      Wait on new data
 *
 * @return     True when finished
 */
static bool microphone_inference_record(void)
{
  bool ret = true;

  while (inference.buf_ready == 0)
  {
    delay(10);
  }

  inference.buf_ready = 0;
  return ret;
}

static int microphone_audio_signal_get_data(size_t offset, size_t length, float *out_ptr)
{
  numpy::int16_to_float(&inference.buffer[offset], out_ptr, length);

  return 0;
}