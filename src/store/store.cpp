#include "store.h"
#include "config.h"
#include "preferences/preferences.h"

#define XSTR(x) #x
#define STR(x) XSTR(x)

Store::Store()
{
    state = State();
}

void Store::begin()
{
    reset();
}

Store::State Store::getState()
{
    return state;
};

void Store::reset()
{
    /*Preferences preferences;
    preferences.begin("zharv-battery", false);

    state.uuid = preferences.getString("uuid", "");

    if (!state.uuid.length() || state.uuid.length() > 36)
    {
        uint8_t uuid_array[16];
        esp_uuid::UUID uuid = esp_uuid::UUID();
        preferences.putString("uuid", uuid.toString());
        state.uuid = preferences.getString("uuid", "");
    }

    // Calculate ID (CRC16 from UUID), need from detect master battery, master is upper value
    state.id = (~crc32_le((uint32_t) ~(0xffffffff), (const uint8_t *)state.uuid.c_str(), 8)) ^ 0xffffffFF;*/
    state.cellsCount = MAX_CELLS_COUNT;

    state.minCellVoltage = 0;
    state.maxCellVoltage = 0;
    state.cellOvTrig = 0;
    state.cellUvTrig = 0;
    state.oVLimit = 0;
    state.uVLimit = 0;

    int i;

    for (i = 0; i < MAX_CELLS_COUNT; i++)
    {
        state.voltages[i] = 0;
    }

    for (i = 0; i < 6; i++)
    {
        state.temps[i] = 0;
    }

    state.cellTemp = 0;
    state.bmsTemp = 0;

    state.chgOtTrig = 0;
    state.chgUtTrig = 0;
    state.dsgOtTrig = 0;
    state.dsgUtTrig = 0;

    state.batteryAvailable = false;
    state.soc = 0.0;
    state.nominalCapacity = 0;
    state.residualCapacity = 0;
    state.soh = 0.0;
    state.voltage = 0.0;
    state.adcExternalVolt = 0.0;
    state.adcVoltDiff = 0.0;
    state.current = 0.0;
    state.chgOCP = 0;
    state.dsgOCP = 0;
    state.ballStartVolt = 0;
    state.preChargeButtonPressed = false;
    state.preChargeEnable = false;
    state.dishargeEnable = false;
    state.chargeEnable = false;
    state.error = false;
    state.needReboot = false;
    state.simulateError = false;
    state.batteryStatus = STATUS_UNKNOWN;
    state.protectionStatus = {};

    // NETWORK
    state.networkAvailable = true;
    state.serverStarted = false;
    state.ip = "";
    state.mac = "";
    state.ssid = "";
    state.password = "";
    state.staticIp = "";
    state.mask = "";
    state.gate = "";
    state.wifiStatus = 0;
    state.timestamp = 0;

    // INVERTER
    state.inverterAcVoltage = 0;
    state.inverterAcPower = 0;
    state.inverterDcVoltage = 0;
    state.inverterTemp = 0;
    state.inverterProtection = 0;
};

String Store::toJson()
{
    DynamicJsonDocument doc(2048);
    int i = 0;
    doc["soh"] = state.soh;
    doc["voltage"] = state.voltage;
    doc["current"] = state.current;
    doc["residialCapacity"] = state.residualCapacity;

    doc["uuid"] = state.uuid;
    doc["id"] = state.id;
    doc["cellsCount"] = state.cellsCount;

    JsonArray cellsVoltage = doc.createNestedArray("voltages");
    for (i = 0; i < state.cellsCount; i++)
    {
        cellsVoltage.add(state.voltages[i]);
    }

    doc["minCellVoltage"] = state.minCellVoltage;
    doc["maxCellVoltage"] = state.maxCellVoltage;
    doc["uVLimit"] = state.uVLimit;
    doc["oVLimit"] = state.oVLimit;

    doc["cellOvTrig"] = state.cellOvTrig;
    doc["cellUvTrig"] = state.cellUvTrig;

    JsonArray tempsVoltage = doc.createNestedArray("temps");
    for (i = 0; i < 6; i++)
    {
        if (state.temps[i] > 0)
            tempsVoltage.add(state.temps[i]);
    }

    doc["chgOtTrig"] = state.chgOtTrig;
    doc["chgUtTrig"] = state.chgUtTrig;
    doc["dsgOtTrig"] = state.dsgOtTrig;
    doc["dsgUtTrig"] = state.dsgUtTrig;
    doc["chgOCP"] = state.chgOCP;
    doc["dsgOCP"] = state.dsgOCP;
    doc["ballStartVolt"] = state.ballStartVolt;

    JsonObject protectionStatus = doc.createNestedObject("protectionStatus");
    protectionStatus["cellOverVol"] = state.protectionStatus.cellOverVol;
    protectionStatus["cellUnderVol"] = state.protectionStatus.cellUnderVol;
    protectionStatus["battOverVol"] = state.protectionStatus.battOverVol;
    protectionStatus["battUnderVol"] = state.protectionStatus.battUnderVol;
    protectionStatus["chargeOverTemp"] = state.protectionStatus.chargeOverTemp;
    protectionStatus["chargeLowTemp"] = state.protectionStatus.chargeLowTemp;
    protectionStatus["dishargeOverTemp"] = state.protectionStatus.dishargeOverTemp;
    protectionStatus["dishargeLowTemp"] = state.protectionStatus.dishargeLowTemp;
    protectionStatus["chargeOverCurrent"] = state.protectionStatus.chargeOverCurrent;
    protectionStatus["dishargeOverCurrent"] = state.protectionStatus.dishargeOverCurrent;
    protectionStatus["shortSircuit"] = state.protectionStatus.shortSircuit;
    protectionStatus["icError"] = state.protectionStatus.icError;
    protectionStatus["mosSoftwareLock"] = state.protectionStatus.mosSoftwareLock;

    doc["cellTemp"] = state.cellTemp;
    doc["bmsTemp"] = state.bmsTemp;
    doc["soc"] = state.soc;
    doc["nominalCapacity"] = state.nominalCapacity;
    doc["adcExternalVolt"] = state.adcExternalVolt;
    doc["cycles"] = state.cycles;
    doc["preChargeEnable"] = state.preChargeEnable;
    doc["dishargeEnable"] = state.dishargeEnable;
    doc["chargeEnable"] = state.chargeEnable;
    ;
    doc["error"] = state.error;
    doc["networkAvailable"] = state.networkAvailable;
    doc["serverStarted"] = state.serverStarted;
    doc["ip"] = state.ip;
    doc["mac"] = state.mac;
    doc["ssid"] = state.ssid;
    // doc["password"] = state.password;
    doc["staticIp"] = state.staticIp;
    doc["mask"] = state.mask;
    doc["gate"] = state.gate;
    // doc["userName"] = state.userName;
    // doc["userPass"] = state.userPassword;
    // doc["canAvailable"] = state.canAvailable;
    // doc["timestamp"] = state.timestamp;
    // doc["updateFirmware"] = state.updateFirmware;
    doc["wifiStatus"] = getWiFiStatusString(state.wifiStatus);
    doc["batteryStatus"] = getBatteryStatusString(state.batteryStatus);

    String output;
    serializeJson(doc, output);
    return output;
}

void Store::_writeStringToEEPROM(int addrOffset, const String &strToWrite)
{
    byte len = strToWrite.length();
    EEPROM.write(addrOffset, len);
    for (int i = 0; i < len; i++)
    {
        EEPROM.write(addrOffset + 1 + i, strToWrite[i]);
    }

    EEPROM.commit();
}

String Store::_readStringFromEEPROM(int addrOffset)
{
    int newStrLen = EEPROM.read(addrOffset);

    char data[newStrLen + 1];
    for (int i = 0; i < newStrLen; i++)
    {
        data[i] = EEPROM.read(addrOffset + 1 + i);
    }
    data[newStrLen] = '\0'; // the character may appear in a weird way, you should read: 'only one backslash and 0'
    return String(data);
}

String Store::getBatteryStatusString(Store::BatteryStatus status)
{
    switch (status)
    {
    case Store::STATUS_IDLE:
        return "IDLE";
    case Store::STATUS_CHARGING:
        return "CHARGING";
    case Store::STATUS_DISCHARGING:
        return "DISCHARGING";
    case Store::STATUS_PRECHARGING:
        return "PRECHARGING";
    case Store::STATUS_UNKNOWN:
        return "UNKNOWN";
    }

    return "UNKNOWN";
}

String Store::getWiFiStatusString(uint8_t status)
{
    switch (status)
    {
    case WL_IDLE_STATUS:
        return "IDLE";
    case WL_NO_SSID_AVAIL:
        return "NO_SSID_AVAIL";
    case WL_SCAN_COMPLETED:
        return "SCAN_COMPLETED";
    case WL_CONNECTED:
        return "CONNECTED";
    case WL_CONNECT_FAILED:
        return "CONNECT_FAILED";
    case WL_CONNECTION_LOST:
        return "CONNECTION_LOST";
    case WL_DISCONNECTED:
        return "DISCONNECTED";
    };

    return "UKNOWN";
};

bool Store::needReboot()
{
    return state.needReboot;
};

void Store::saveAndReboot()
{
    Serial.println("STORE: Save and reboot");
    ESP.restart();
};
