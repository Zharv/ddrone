#ifndef State_h
#define State_h

#include <Arduino.h>
#include <EEPROM.h>
#include <WiFi.h>
#include <ArduinoJson.h>
#include <esp32/rom/crc.h>

#include "config.h"

class Store
{
public:
    struct ProtectionStatus
    {
        bool cellOverVol;
        bool cellUnderVol;
        bool battOverVol;
        bool battUnderVol;
        bool chargeOverTemp;
        bool chargeLowTemp;
        bool dishargeOverTemp;
        bool dishargeLowTemp;
        bool chargeOverCurrent;
        bool dishargeOverCurrent;
        bool shortSircuit;
        bool icError;
        bool mosSoftwareLock;
    };

    typedef enum
    {
        STATUS_IDLE = 1,
        STATUS_CHARGING = 2,
        STATUS_DISCHARGING = 3,
        STATUS_PRECHARGING = 4,
        STATUS_UNKNOWN = 0
    } BatteryStatus;

    struct SOHData
    {
        float max;
        float min;
        float current;
        float values[5];
        int pos;
    };

    typedef struct
    {
        String uuid;
        uint16_t id;
        // CELLS
        int cellsCount;
        uint16_t voltages[21];
        uint16_t minCellVoltage;
        uint16_t maxCellVoltage;
        uint16_t cellsDiff;
        uint16_t cellsAvg;
        uint16_t cellOvTrig;
        uint16_t cellUvTrig;
        uint16_t uVLimit;
        uint16_t oVLimit;

        // TEMP
        uint16_t temps[6];
        uint16_t cellTemp;
        uint16_t bmsTemp;
        uint16_t chgOtTrig;
        uint16_t chgUtTrig;
        uint16_t dsgOtTrig;
        uint16_t dsgUtTrig;

        // BATTERY
        String batteryType;
        bool batteryAvailable;
        uint16_t soc;
        uint16_t nominalCapacity;
        uint16_t residualCapacity;
        uint16_t soh;
        uint16_t voltage;
        uint16_t cycles;
        uint16_t adcExternalVolt;
        uint16_t adcVoltDiff;
        int16_t current;
        uint16_t chgOCP;
        uint16_t dsgOCP;
        uint16_t ballStartVolt;
        bool dishargeEnable;
        bool chargeEnable;
        bool preChargeEnable;
        bool preChargeButtonPressed;
        bool error;
        bool needReboot;
        bool simulateError;
        BatteryStatus batteryStatus;
        ProtectionStatus protectionStatus;

        // NETWORK
        bool networkAvailable;
        bool serverStarted;
        String ip;
        String mac;
        String ssid;
        String password;
        String staticIp;
        String mask;
        String gate;
        uint8_t wifiStatus;
        unsigned long timestamp;

        // AUTH
        String userName;
        String userPassword;

        //INVERTER
        int inverterAcVoltage;
        int inverterAcPower;
        int inverterDcVoltage;
        int inverterTemp;
        int inverterProtection;

    } State;

    State state;

    Store();
    void begin();
    void reset();
    void saveAndReboot();

    bool needReboot();

    String toJson();
    String getBatteryStatusString(BatteryStatus status);
    String getWiFiStatusString(uint8_t status);

    State getState();

private:
    SOHData _sohData;

    String _readStringFromEEPROM(int addrOffset);
    void _writeStringToEEPROM(int addrOffset, const String &strToWrite);
};

#endif