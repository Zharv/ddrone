import numpy as np
import librosa
import os
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import tensorflow as tf
from keras.layers import Conv2D, MaxPooling2D, Flatten, Dense, Dropout, BatchNormalization

# Загрузка данных и меток
def load_data(data_folder):
    mfcc_features = []
    labels = []
    for file in os.listdir(data_folder):
        if file.endswith(".wav"):
            file_path = os.path.join(data_folder, file)
            audio_data, _ = librosa.load(file_path)
            mfcc = librosa.feature.mfcc(y=audio_data, n_mfcc=13).mean(axis=1)
            mfcc_features.append(mfcc)
            if "DRONE" in file:
                labels.append(0)
            else:
                labels.append(1)
    return np.array(mfcc_features), np.array(labels)

def create_model(input_shape):
    model = Sequential()
    model.add(Dense(64, activation='relu', input_shape=input_shape))
    model.add(Dropout(0.5))
    model.add(Dense(32, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(2, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])
    return model

def convert_to_tflite(model, input_shape):
    concrete_func = tf.function(lambda x: model(x))
    concrete_func = concrete_func.get_concrete_function(tf.TensorSpec([1, *input_shape], tf.float32))
    converter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_func])

    converter.optimizations = [tf.lite.Optimize.DEFAULT]

    tflite_model = converter.convert()

    with open('model.tflite', 'wb') as f:
        f.write(tflite_model)


# Загрузка данных
data_folder = 'dataset'
X, y = load_data(data_folder)

# Разделение данных на обучающую и тестовую выборки
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Преобразование меток в категориальный формат
y_train = to_categorical(y_train, num_classes=2)
y_test = to_categorical(y_test, num_classes=2)

# Создание и обучение модели
input_shape = (X_train.shape[1],)
model = create_model(input_shape)
model.fit(X_train, y_train, epochs=100, batch_size=32, validation_data=(X_test, y_test))

# Сохранение модели
model.save('drone_detector_mfcc.h5')
convert_to_tflite(model, input_shape)