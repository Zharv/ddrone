import math

SAMPLE_RATE = 44100
FRAME_SIZE = 512
NUM_FILTER_BANKS = 26
NUM_DCT_COEFFS = 13


def init_mel_filter_banks():
    mel_filter_banks = [[0] * (FRAME_SIZE // 2) for _ in range(NUM_FILTER_BANKS)]
    for i in range(NUM_FILTER_BANKS):
        for j in range(FRAME_SIZE // 2):
            mel_center_freq = 700 * (10 ** ((i + 1) / 2595.0) - 1)
            mel_lower_freq = 700 * (10 ** (i / 2595.0) - 1)
            mel_upper_freq = 700 * (10 ** ((i + 2) / 2595.0) - 1)
            triangle_value = 0
            if mel_lower_freq <= j <= mel_center_freq:
                triangle_value = (j - mel_lower_freq) / (mel_center_freq - mel_lower_freq)
            elif mel_center_freq <= j <= mel_upper_freq:
                triangle_value = (mel_upper_freq - j) / (mel_upper_freq - mel_center_freq)
            mel_filter_banks[i][j] = triangle_value
    return mel_filter_banks


def init_dct_coefficients():
    dct_coefficients = [[0] * NUM_FILTER_BANKS for _ in range(NUM_DCT_COEFFS)]
    for i in range(NUM_DCT_COEFFS):
        for j in range(NUM_FILTER_BANKS):
            dct_coefficients[i][j] = math.cos((i * math.pi / NUM_FILTER_BANKS) * (j + 0.5))
    return dct_coefficients


def print_float_matrix(matrix, name):
    print(f"const float {name}[{len(matrix)}][{len(matrix[0])}] = {{")
    for row in matrix:
        print("    {" + ", ".join(f"{value:.10f}" for value in row) + "},")
    print("};")


mel_filter_banks = init_mel_filter_banks()
dct_coefficients = init_dct_coefficients()

print_float_matrix(mel_filter_banks, "mel_filter_banks")
print_float_matrix(dct_coefficients, "dct_coefficients")
