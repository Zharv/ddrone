import os
import librosa
import numpy as np
from keras.models import load_model

# Загрузка модели
model = load_model('drone_detector_mfcc.h5')

# Извлечение MFCC-признаков из аудиофайла
def extract_mfcc(file_path):
    audio_data, _ = librosa.load(file_path)
    mfcc = librosa.feature.mfcc(y=audio_data, n_mfcc=13).mean(axis=1)
    return mfcc.reshape(1, -1)

# Тестирование модели на аудиофайлах из датасета
data_folder = 'dataset'
for file in os.listdir(data_folder):
    if file.endswith(".wav"):
        file_path = os.path.join(data_folder, file)
        mfcc_features = extract_mfcc(file_path)
        y_pred_proba = model.predict(mfcc_features)
        y_pred_class = np.argmax(y_pred_proba, axis=1)

        if y_pred_class[0] == 0:
            predicted_label = "Drone"
        else:
            predicted_label = "NoDrone"

        # Получение вероятности для предсказанного класса и преобразование в проценты
        class_proba = y_pred_proba[0][y_pred_class[0]] * 100

        print("File: {}, Predicted: {}, Probability: {:.2f}%".format(file, predicted_label, class_proba))