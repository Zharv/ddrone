#python tflite_to_c.py drone_detector_quantized_model.tflite model.h drone_detector_quantized_model

import sys

def tflite_to_model_h(tflite_file, output_file, array_name):
    with open(tflite_file, 'rb') as f:
        content = f.read()

    with open(output_file, 'w') as f:
        f.write(f"#ifndef {array_name.upper()}_H\n")
        f.write(f"#define {array_name.upper()}_H\n\n")
        f.write("#include <cstdint>\n\n")
        f.write(f"const unsigned char {array_name}[] = {{")
        for i, byte in enumerate(content):
            f.write(f"{hex(byte)}")
            if i < len(content) - 1:
                f.write(", ")
            if (i + 1) % 16 == 0:
                f.write("\n")
        f.write("};\n\n")
        f.write(f"#endif  // {array_name.upper()}_H\n")

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print("Usage: python tflite_to_model_h.py <tflite_file> <output_file> <array_name>")
    else:
        tflite_to_model_h(sys.argv[1], sys.argv[2], sys.argv[3])
