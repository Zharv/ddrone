import tensorflow as tf

model_path = 'drone_detector_raw_audio.h5'
tflite_model_path = 'drone_detector_raw_audio.tflite'

# Загрузка модели
model = tf.keras.models.load_model(model_path)

# Конвертация модели в формат TFLite
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Сохранение TFLite модели
with open(tflite_model_path, 'wb') as f:
    f.write(tflite_model)