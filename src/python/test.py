import librosa
import numpy as np
import tensorflow as tf
from keras.models import load_model


def predict_audio_class(model, audio_data):
    audio_data = audio_data[np.newaxis, :]
    predictions = model.predict(audio_data)
    return np.argmax(predictions, axis=1)


def main():
    model = load_model('drone_detector_model_44100hz.h5')
    audio_file = 'dataset/HELICOPTER_005.wav'

    signal, sr = librosa.load(audio_file, sr=44100, mono=True)
    signal = signal.astype(np.float32) / np.iinfo(np.int16).max

    n_fft = 64
    hop_length = 64

    if len(signal) >= n_fft:
        spectrogram = librosa.stft(signal, n_fft=n_fft, hop_length=hop_length)
        spectrogram = np.abs(spectrogram)

        # Ensure that the spectrogram shape matches the model's input shape
        input_shape = model.layers[0].input_shape[1:3]
        max_len = input_shape[1]

        if spectrogram.shape[1] < max_len:
            spectrogram = np.pad(spectrogram, ((0, 0), (0, max_len - spectrogram.shape[1])))

        prediction = predict_audio_class(model, spectrogram)
        print(f"Predicted class: {prediction}")
    else:
        print("Signal is too short")


if __name__ == '__main__':
    main()
