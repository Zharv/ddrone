import os
import numpy as np
from numpy.fft import rfft
import librosa
import tensorflow as tf
from sklearn.model_selection import train_test_split
from keras.models import Sequential
from keras.layers import SeparableConv2D, GlobalAveragePooling2D, Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Reshape, Input, DepthwiseConv2D
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder

# Загрузка данных
dataset_path = "DroneAudioDataset/Multiclass_Drone_Audio"
categories = ["bebop_1", "unknown"]

audio_data = []
audio_labels = []

for category in categories:
    category_path = os.path.join(dataset_path, category)
    for file in os.listdir(category_path):
        file_path = os.path.join(category_path, file)
        signal, _ = librosa.load(file_path, sr=44100)
        fft_result = rfft(signal)
        audio_data.append(fft_result)
        audio_labels.append(category)

# Вычислите максимальную длину одномерного массива после применения FFT
max_len = max([fft_result.shape[0] for fft_result in audio_data])

# Дополните все одномерные массивы до максимальной длины
audio_data_padded = []
for fft_result in audio_data:
    padded_fft_result = np.pad(fft_result, (0, max_len - fft_result.shape[0]))
    audio_data_padded.append(padded_fft_result)

audio_data = np.stack(audio_data_padded)


# Разделение данных на обучающую и тестовую выборки
train_data, test_data, train_labels, test_labels = train_test_split(audio_data, audio_labels, test_size=0.2, random_state=42)

# Создание модели
input_shape = audio_data[0].shape
model = Sequential([
    Input(shape=input_shape),
    Reshape((*input_shape, 1)),  # Add an extra dimension for the channel
    DepthwiseConv2D(kernel_size=(3, 3), activation="relu"),
    MaxPooling2D(pool_size=(2, 2)),
    Dropout(0.25),
    DepthwiseConv2D(kernel_size=(3, 3), activation="relu"),
    MaxPooling2D(pool_size=(2, 2)),
    Dropout(0.25),
    GlobalAveragePooling2D(),
    Dense(16, activation="relu"),
    Dropout(0.5),
    Dense(2, activation="softmax"),
])

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["accuracy"])

model.summary()

# Обучение модели
batch_size = 16
epochs = 10

history = model.fit(train_data, train_labels, batch_size=batch_size, epochs=epochs, validation_data=(test_data, test_labels))

# Оценка модели
test_loss, test_accuracy = model.evaluate(test_data, test_labels)
print(f"Тестовая потеря: {test_loss:.4f}")
print(f"Тестовая точность: {test_accuracy:.4f}")

# Сохранение модели
model.save("drone_detector_model_44100hz.h5")

# Конвертация модели в TensorFlow Lite формат
# Создайте конвертер и укажите модель
converter = tf.lite.TFLiteConverter.from_keras_model(model)

# Включите квантизацию пост-тренировки
converter.optimizations = [tf.lite.Optimize.DEFAULT]

# Устанавливаем квантизацию в 8 бит
converter.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS_INT8]
converter.inference_input_type = tf.int8
converter.inference_output_type = tf.int8

# Подготовка данных для квантизации
def representative_dataset():
    for i in range(len(train_data)):
        yield [train_data[i:i+1].astype(np.float32)]

converter.representative_dataset = representative_dataset

tflite_model = converter.convert()

# Сохранение TensorFlow Lite модели
with open("drone_detector_model_44100hz.tflite", "wb") as f:
    f.write(tflite_model)

print("Модель успешно конвертирована и сохранена в формате TensorFlow Lite.")
