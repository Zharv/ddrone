import os
import time
import numpy as np
import sounddevice as sd
from pydub import AudioSegment
from pydub.playback import play
from scipy.io.wavfile import read, write

# Папки ввода и вывода
input_folder = 'DroneAudioDataset/Binary_Drone_Audio/yes_drone'
output_folder = 'dataset/drone'

# Получение списка файлов wav в папке ввода
wav_files = [f for f in os.listdir(input_folder) if f.endswith('.wav')]

# Ограничиваем количество файлов до 1100
wav_files = wav_files[:1100]

# Объединение всех аудиофайлов в одну дорожку
combined_audio = AudioSegment.empty()
for wav_file in wav_files:
    audio = AudioSegment.from_wav(os.path.join(input_folder, wav_file))
    combined_audio += audio

# Вычисляем продолжительность аудио в секундах
duration = len(combined_audio) / 1000.0

# Настройка и начало записи
recording = sd.rec(int(duration * 44100), samplerate=44100, channels=1)

# Воспроизведение файла
play(combined_audio)

sd.wait()

# Преобразование массива numpy в формат, пригодный для записи в wav файл
recording_int16 = np.int16(recording[:, 0]*32767)

# Разделение аудио на секундные сегменты и сохранение каждого сегмента в файл
for i in range(int(duration)):
    start = i * 44100  # начало сегмента в сэмплах
    end = (i + 1) * 44100  # конец сегмента в сэмплах
    segment = recording_int16[start:end]
    write(os.path.join(output_folder, f'rec_{i}.wav'), 44100, segment)
