// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.2.1
// LVGL VERSION: 8.2.0
// PROJECT: SquareLine_Project

#ifndef _SQUARELINE_PROJECT_UI_H
#define _SQUARELINE_PROJECT_UI_H

#ifdef __cplusplus
extern "C" {
#endif

#include "lvgl.h"

extern lv_obj_t * ui_SplashScreen;
extern lv_obj_t * ui_Image1;
extern lv_obj_t * ui_Panel1;
extern lv_obj_t * ui_Spinner2;
extern lv_obj_t * ui_Spinner1;
extern lv_obj_t * ui_Image2;
extern lv_obj_t * ui_MainScreen;
extern lv_obj_t * ui_Panel2;
extern lv_obj_t * ui_BatteryCapacityLabel;
extern lv_obj_t * ui_BatteryCurrentLabel;
extern lv_obj_t * ui_BatteryVoltageLabel;
extern lv_obj_t * ui_InvertorSwich;
extern lv_obj_t * ui_BatterySwich;
extern lv_obj_t * ui_Label3;
extern lv_obj_t * ui_Label1;
extern lv_obj_t * ui_BatteryInfoPanel;
extern lv_obj_t * ui_Label16;
extern lv_obj_t * ui_Label12;
extern lv_obj_t * ui_BatteryBalanceLabel;
extern lv_obj_t * ui_RemainingTimeLabel;
extern lv_obj_t * ui_Label10;
extern lv_obj_t * ui_BatteryTemperatureLabel;
extern lv_obj_t * ui_InvertorInfoPanel;
extern lv_obj_t * ui_Label4;
extern lv_obj_t * ui_InvertorVoltageLabel;
extern lv_obj_t * ui_Label5;
extern lv_obj_t * ui_InvertorPowerLabel;
extern lv_obj_t * ui_Label8;
extern lv_obj_t * ui_InvertorTemperatureLabel;
extern lv_obj_t * ui_InvertorPowerIndicator;
extern lv_obj_t * ui_BatteryPowerIndicator;


LV_IMG_DECLARE(ui_img_fon_png);    // assets\fon.png
LV_IMG_DECLARE(ui_img_gerb4_png);    // assets\gerb4.png
LV_IMG_DECLARE(ui_img_power_icon_png);    // assets\power_icon.png
LV_IMG_DECLARE(ui_img_battery_icon_png);    // assets\battery_icon.png


LV_FONT_DECLARE(ui_font_RobotoBold16);


void ui_init(void);

#ifdef __cplusplus
} /*extern "C"*/
#endif

#endif
