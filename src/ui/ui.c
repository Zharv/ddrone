// SquareLine LVGL GENERATED FILE
// EDITOR VERSION: SquareLine Studio 1.2.1
// LVGL VERSION: 8.2.0
// PROJECT: SquareLine_Project

#include "ui.h"
#include "ui_helpers.h"

///////////////////// VARIABLES ////////////////////
lv_obj_t * ui_SplashScreen;
lv_obj_t * ui_Image1;
lv_obj_t * ui_Panel1;
lv_obj_t * ui_Spinner2;
lv_obj_t * ui_Spinner1;
lv_obj_t * ui_Image2;
lv_obj_t * ui_MainScreen;
lv_obj_t * ui_Panel2;
lv_obj_t * ui_BatteryCapacityLabel;
lv_obj_t * ui_BatteryCurrentLabel;
lv_obj_t * ui_BatteryVoltageLabel;
lv_obj_t * ui_InvertorSwich;
lv_obj_t * ui_BatterySwich;
lv_obj_t * ui_Label3;
lv_obj_t * ui_Label1;
lv_obj_t * ui_BatteryInfoPanel;
lv_obj_t * ui_Label16;
lv_obj_t * ui_Label12;
lv_obj_t * ui_BatteryBalanceLabel;
lv_obj_t * ui_RemainingTimeLabel;
lv_obj_t * ui_Label10;
lv_obj_t * ui_BatteryTemperatureLabel;
lv_obj_t * ui_InvertorInfoPanel;
lv_obj_t * ui_Label4;
lv_obj_t * ui_InvertorVoltageLabel;
lv_obj_t * ui_Label5;
lv_obj_t * ui_InvertorPowerLabel;
lv_obj_t * ui_Label8;
lv_obj_t * ui_InvertorTemperatureLabel;
lv_obj_t * ui_InvertorPowerIndicator;
lv_obj_t * ui_BatteryPowerIndicator;

///////////////////// TEST LVGL SETTINGS ////////////////////
#if LV_COLOR_DEPTH != 16
    #error "LV_COLOR_DEPTH should be 16bit to match SquareLine Studio's settings"
#endif
#if LV_COLOR_16_SWAP !=1
    #error "LV_COLOR_16_SWAP should be 1 to match SquareLine Studio's settings"
#endif

///////////////////// ANIMATIONS ////////////////////

///////////////////// FUNCTIONS ////////////////////

///////////////////// SCREENS ////////////////////
void ui_SplashScreen_screen_init(void)
{
    ui_SplashScreen = lv_obj_create(NULL);
    lv_obj_clear_flag(ui_SplashScreen, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_Image1 = lv_img_create(ui_SplashScreen);
    lv_img_set_src(ui_Image1, &ui_img_fon_png);
    lv_obj_set_width(ui_Image1, LV_SIZE_CONTENT);   /// 480
    lv_obj_set_height(ui_Image1, LV_SIZE_CONTENT);    /// 320
    lv_obj_set_align(ui_Image1, LV_ALIGN_CENTER);
    lv_obj_add_flag(ui_Image1, LV_OBJ_FLAG_ADV_HITTEST);     /// Flags
    lv_obj_clear_flag(ui_Image1, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

    ui_Panel1 = lv_obj_create(ui_SplashScreen);
    lv_obj_set_width(ui_Panel1, 170);
    lv_obj_set_height(ui_Panel1, 170);
    lv_obj_set_x(ui_Panel1, 0);
    lv_obj_set_y(ui_Panel1, 20);
    lv_obj_set_align(ui_Panel1, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_Panel1, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_radius(ui_Panel1, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_Panel1, lv_color_hex(0x282828), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Panel1, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_Panel1, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_Panel1, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Spinner2 = lv_spinner_create(ui_SplashScreen, 1000, 90);
    lv_obj_set_width(ui_Spinner2, 170);
    lv_obj_set_height(ui_Spinner2, 170);
    lv_obj_set_x(ui_Spinner2, 0);
    lv_obj_set_y(ui_Spinner2, 20);
    lv_obj_set_align(ui_Spinner2, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_Spinner2, LV_OBJ_FLAG_CLICKABLE);      /// Flags
    lv_obj_set_style_arc_color(ui_Spinner2, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_Spinner2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_width(ui_Spinner2, 5, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_arc_width(ui_Spinner2, 5, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    ui_Spinner1 = lv_spinner_create(ui_SplashScreen, 1000, 90);
    lv_obj_set_width(ui_Spinner1, 150);
    lv_obj_set_height(ui_Spinner1, 150);
    lv_obj_set_x(ui_Spinner1, 0);
    lv_obj_set_y(ui_Spinner1, 20);
    lv_obj_set_align(ui_Spinner1, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_Spinner1, LV_OBJ_FLAG_CLICKABLE);      /// Flags
    lv_obj_set_style_arc_color(ui_Spinner1, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_Spinner1, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_width(ui_Spinner1, 5, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_arc_width(ui_Spinner1, 5, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    ui_Image2 = lv_img_create(ui_SplashScreen);
    lv_img_set_src(ui_Image2, &ui_img_gerb4_png);
    lv_obj_set_width(ui_Image2, LV_SIZE_CONTENT);   /// 100
    lv_obj_set_height(ui_Image2, LV_SIZE_CONTENT);    /// 100
    lv_obj_set_x(ui_Image2, 0);
    lv_obj_set_y(ui_Image2, 20);
    lv_obj_set_align(ui_Image2, LV_ALIGN_CENTER);
    lv_obj_add_flag(ui_Image2, LV_OBJ_FLAG_ADV_HITTEST);     /// Flags
    lv_obj_clear_flag(ui_Image2, LV_OBJ_FLAG_SCROLLABLE);      /// Flags

}
void ui_MainScreen_screen_init(void)
{
    ui_MainScreen = lv_obj_create(NULL);
    lv_obj_add_flag(ui_MainScreen, LV_OBJ_FLAG_CHECKABLE);     /// Flags
    lv_obj_clear_flag(ui_MainScreen, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_bg_img_src(ui_MainScreen, &ui_img_fon_png, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_color(ui_MainScreen, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_opa(ui_MainScreen, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_width(ui_MainScreen, 3, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_shadow_spread(ui_MainScreen, 3, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Panel2 = lv_obj_create(ui_MainScreen);
    lv_obj_set_width(ui_Panel2, 170);
    lv_obj_set_height(ui_Panel2, 170);
    lv_obj_set_x(ui_Panel2, 0);
    lv_obj_set_y(ui_Panel2, 20);
    lv_obj_set_align(ui_Panel2, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_Panel2, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_radius(ui_Panel2, 150, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_color(ui_Panel2, lv_color_hex(0x282828), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_Panel2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_color(ui_Panel2, lv_color_hex(0x000000), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_opa(ui_Panel2, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryCapacityLabel = lv_label_create(ui_MainScreen);
    lv_obj_set_width(ui_BatteryCapacityLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_BatteryCapacityLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_BatteryCapacityLabel, 0);
    lv_obj_set_y(ui_BatteryCapacityLabel, 20);
    lv_obj_set_align(ui_BatteryCapacityLabel, LV_ALIGN_CENTER);
    lv_label_set_text(ui_BatteryCapacityLabel, "0%");
    lv_obj_set_style_text_color(ui_BatteryCapacityLabel, lv_color_hex(0xC3C3C3), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_BatteryCapacityLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_BatteryCapacityLabel, &lv_font_montserrat_38, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryCurrentLabel = lv_label_create(ui_MainScreen);
    lv_obj_set_width(ui_BatteryCurrentLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_BatteryCurrentLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_BatteryCurrentLabel, 0);
    lv_obj_set_y(ui_BatteryCurrentLabel, -30);
    lv_obj_set_align(ui_BatteryCurrentLabel, LV_ALIGN_CENTER);
    lv_label_set_text(ui_BatteryCurrentLabel, "0A");
    lv_obj_set_style_text_color(ui_BatteryCurrentLabel, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_BatteryCurrentLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_BatteryCurrentLabel, &lv_font_montserrat_16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryVoltageLabel = lv_label_create(ui_MainScreen);
    lv_obj_set_width(ui_BatteryVoltageLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_BatteryVoltageLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_BatteryVoltageLabel, 0);
    lv_obj_set_y(ui_BatteryVoltageLabel, 70);
    lv_obj_set_align(ui_BatteryVoltageLabel, LV_ALIGN_CENTER);
    lv_label_set_text(ui_BatteryVoltageLabel, "0B");
    lv_obj_set_style_text_color(ui_BatteryVoltageLabel, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_BatteryVoltageLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_BatteryVoltageLabel, &lv_font_montserrat_16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorSwich = lv_switch_create(ui_MainScreen);
    lv_obj_set_width(ui_InvertorSwich, 90);
    lv_obj_set_height(ui_InvertorSwich, 50);
    lv_obj_set_x(ui_InvertorSwich, -70);
    lv_obj_set_y(ui_InvertorSwich, -120);
    lv_obj_set_align(ui_InvertorSwich, LV_ALIGN_CENTER);
    lv_obj_set_style_bg_color(ui_InvertorSwich, lv_color_hex(0x464646), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_InvertorSwich, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_InvertorSwich, lv_color_hex(0x646464), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_InvertorSwich, 255, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_img_src(ui_InvertorSwich, &ui_img_power_icon_png, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_img_opa(ui_InvertorSwich, 150, LV_PART_KNOB | LV_STATE_DEFAULT);

    ui_BatterySwich = lv_switch_create(ui_MainScreen);
    lv_obj_set_width(ui_BatterySwich, 90);
    lv_obj_set_height(ui_BatterySwich, 50);
    lv_obj_set_x(ui_BatterySwich, 70);
    lv_obj_set_y(ui_BatterySwich, -120);
    lv_obj_set_align(ui_BatterySwich, LV_ALIGN_CENTER);
    lv_obj_set_style_bg_color(ui_BatterySwich, lv_color_hex(0x464646), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_BatterySwich, 255, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_BatterySwich, lv_color_hex(0x646464), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_BatterySwich, 255, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_img_src(ui_BatterySwich, &ui_img_battery_icon_png, LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_img_opa(ui_BatterySwich, 150, LV_PART_KNOB | LV_STATE_DEFAULT);

    ui_Label3 = lv_label_create(ui_MainScreen);
    lv_obj_set_width(ui_Label3, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label3, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label3, -30);
    lv_obj_set_y(ui_Label3, -121);
    lv_obj_set_align(ui_Label3, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_Label3, "Батарея");
    lv_obj_set_style_text_color(ui_Label3, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label3, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label3, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label1 = lv_label_create(ui_MainScreen);
    lv_obj_set_width(ui_Label1, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label1, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label1, 24);
    lv_obj_set_y(ui_Label1, -123);
    lv_obj_set_align(ui_Label1, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_Label1, "Інвертор");
    lv_obj_set_style_text_color(ui_Label1, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label1, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label1, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryInfoPanel = lv_obj_create(ui_MainScreen);
    lv_obj_set_width(ui_BatteryInfoPanel, 177);
    lv_obj_set_height(ui_BatteryInfoPanel, 200);
    lv_obj_set_x(ui_BatteryInfoPanel, 0);
    lv_obj_set_y(ui_BatteryInfoPanel, 40);
    lv_obj_set_align(ui_BatteryInfoPanel, LV_ALIGN_RIGHT_MID);
    lv_obj_clear_flag(ui_BatteryInfoPanel, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_bg_color(ui_BatteryInfoPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_BatteryInfoPanel, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_BatteryInfoPanel, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label16 = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_Label16, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label16, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label16, 0);
    lv_obj_set_y(ui_Label16, -92);
    lv_obj_set_align(ui_Label16, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_Label16, "Розбаланс, мВ");
    lv_obj_set_style_text_color(ui_Label16, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label16, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label16, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label12 = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_Label12, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label12, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label12, 0);
    lv_obj_set_y(ui_Label12, -22);
    lv_obj_set_align(ui_Label12, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_Label12, "Час роботи, хв");
    lv_obj_set_style_text_color(ui_Label12, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label12, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label12, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryBalanceLabel = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_BatteryBalanceLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_BatteryBalanceLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_BatteryBalanceLabel, 0);
    lv_obj_set_y(ui_BatteryBalanceLabel, -65);
    lv_obj_set_align(ui_BatteryBalanceLabel, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_BatteryBalanceLabel, "0");
    lv_obj_set_style_text_color(ui_BatteryBalanceLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_BatteryBalanceLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_BatteryBalanceLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_RemainingTimeLabel = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_RemainingTimeLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_RemainingTimeLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_RemainingTimeLabel, 0);
    lv_obj_set_y(ui_RemainingTimeLabel, 5);
    lv_obj_set_align(ui_RemainingTimeLabel, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_RemainingTimeLabel, "0");
    lv_obj_set_style_text_color(ui_RemainingTimeLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_RemainingTimeLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_RemainingTimeLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label10 = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_Label10, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label10, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label10, 0);
    lv_obj_set_y(ui_Label10, 53);
    lv_obj_set_align(ui_Label10, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_Label10, "Температурара\nбатареЇ, С");
    lv_obj_set_style_text_color(ui_Label10, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label10, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label10, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_BatteryTemperatureLabel = lv_label_create(ui_BatteryInfoPanel);
    lv_obj_set_width(ui_BatteryTemperatureLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_BatteryTemperatureLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_BatteryTemperatureLabel, 0);
    lv_obj_set_y(ui_BatteryTemperatureLabel, 92);
    lv_obj_set_align(ui_BatteryTemperatureLabel, LV_ALIGN_RIGHT_MID);
    lv_label_set_text(ui_BatteryTemperatureLabel, "0");
    lv_obj_set_style_text_color(ui_BatteryTemperatureLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_BatteryTemperatureLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_BatteryTemperatureLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorInfoPanel = lv_obj_create(ui_MainScreen);
    lv_obj_set_width(ui_InvertorInfoPanel, 177);
    lv_obj_set_height(ui_InvertorInfoPanel, 200);
    lv_obj_set_x(ui_InvertorInfoPanel, -150);
    lv_obj_set_y(ui_InvertorInfoPanel, 40);
    lv_obj_set_align(ui_InvertorInfoPanel, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_InvertorInfoPanel, LV_OBJ_FLAG_SCROLLABLE);      /// Flags
    lv_obj_set_style_bg_color(ui_InvertorInfoPanel, lv_color_hex(0xFFFFFF), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_InvertorInfoPanel, 0, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_border_width(ui_InvertorInfoPanel, 0, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label4 = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_Label4, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label4, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label4, 0);
    lv_obj_set_y(ui_Label4, -22);
    lv_obj_set_align(ui_Label4, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_Label4, "Напруга, В");
    lv_obj_set_style_text_color(ui_Label4, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label4, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label4, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorVoltageLabel = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_InvertorVoltageLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_InvertorVoltageLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_InvertorVoltageLabel, 0);
    lv_obj_set_y(ui_InvertorVoltageLabel, 5);
    lv_obj_set_align(ui_InvertorVoltageLabel, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_InvertorVoltageLabel, "0");
    lv_obj_set_style_text_color(ui_InvertorVoltageLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_InvertorVoltageLabel, 250, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_InvertorVoltageLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label5 = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_Label5, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label5, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label5, 0);
    lv_obj_set_y(ui_Label5, -92);
    lv_obj_set_align(ui_Label5, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_Label5, "Потужність, Вт");
    lv_obj_set_style_text_color(ui_Label5, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label5, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label5, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorPowerLabel = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_InvertorPowerLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_InvertorPowerLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_InvertorPowerLabel, 0);
    lv_obj_set_y(ui_InvertorPowerLabel, -65);
    lv_obj_set_align(ui_InvertorPowerLabel, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_InvertorPowerLabel, "0");
    lv_obj_set_style_text_color(ui_InvertorPowerLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_InvertorPowerLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_InvertorPowerLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_Label8 = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_Label8, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_Label8, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_Label8, 0);
    lv_obj_set_y(ui_Label8, 53);
    lv_obj_set_align(ui_Label8, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_Label8, "Температура \nінвертору, С");
    lv_obj_set_style_text_color(ui_Label8, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_Label8, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_Label8, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorTemperatureLabel = lv_label_create(ui_InvertorInfoPanel);
    lv_obj_set_width(ui_InvertorTemperatureLabel, LV_SIZE_CONTENT);   /// 1
    lv_obj_set_height(ui_InvertorTemperatureLabel, LV_SIZE_CONTENT);    /// 1
    lv_obj_set_x(ui_InvertorTemperatureLabel, 0);
    lv_obj_set_y(ui_InvertorTemperatureLabel, 92);
    lv_obj_set_align(ui_InvertorTemperatureLabel, LV_ALIGN_LEFT_MID);
    lv_label_set_text(ui_InvertorTemperatureLabel, "0");
    lv_obj_set_style_text_color(ui_InvertorTemperatureLabel, lv_color_hex(0x1E96FA), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_opa(ui_InvertorTemperatureLabel, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_text_font(ui_InvertorTemperatureLabel, &ui_font_RobotoBold16, LV_PART_MAIN | LV_STATE_DEFAULT);

    ui_InvertorPowerIndicator = lv_arc_create(ui_MainScreen);
    lv_obj_set_width(ui_InvertorPowerIndicator, 170);
    lv_obj_set_height(ui_InvertorPowerIndicator, 170);
    lv_obj_set_x(ui_InvertorPowerIndicator, 0);
    lv_obj_set_y(ui_InvertorPowerIndicator, 20);
    lv_obj_set_align(ui_InvertorPowerIndicator, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_InvertorPowerIndicator,
                      LV_OBJ_FLAG_CLICKABLE | LV_OBJ_FLAG_PRESS_LOCK | LV_OBJ_FLAG_CLICK_FOCUSABLE | LV_OBJ_FLAG_GESTURE_BUBBLE |
                      LV_OBJ_FLAG_SNAPPABLE | LV_OBJ_FLAG_SCROLLABLE | LV_OBJ_FLAG_SCROLL_ELASTIC | LV_OBJ_FLAG_SCROLL_MOMENTUM |
                      LV_OBJ_FLAG_SCROLL_CHAIN);     /// Flags
    lv_arc_set_bg_angles(ui_InvertorPowerIndicator, 270, 629);
    lv_obj_set_style_arc_color(ui_InvertorPowerIndicator, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_InvertorPowerIndicator, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_width(ui_InvertorPowerIndicator, 5, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_arc_width(ui_InvertorPowerIndicator, 5, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_InvertorPowerIndicator, lv_color_hex(0xFFFFFF), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_InvertorPowerIndicator, 0, LV_PART_KNOB | LV_STATE_DEFAULT);

    ui_BatteryPowerIndicator = lv_arc_create(ui_MainScreen);
    lv_obj_set_width(ui_BatteryPowerIndicator, 150);
    lv_obj_set_height(ui_BatteryPowerIndicator, 150);
    lv_obj_set_x(ui_BatteryPowerIndicator, 0);
    lv_obj_set_y(ui_BatteryPowerIndicator, 20);
    lv_obj_set_align(ui_BatteryPowerIndicator, LV_ALIGN_CENTER);
    lv_obj_clear_flag(ui_BatteryPowerIndicator,
                      LV_OBJ_FLAG_CLICKABLE | LV_OBJ_FLAG_PRESS_LOCK | LV_OBJ_FLAG_CLICK_FOCUSABLE | LV_OBJ_FLAG_GESTURE_BUBBLE |
                      LV_OBJ_FLAG_SNAPPABLE | LV_OBJ_FLAG_SCROLLABLE | LV_OBJ_FLAG_SCROLL_ELASTIC | LV_OBJ_FLAG_SCROLL_MOMENTUM |
                      LV_OBJ_FLAG_SCROLL_CHAIN);     /// Flags
    lv_arc_set_bg_angles(ui_BatteryPowerIndicator, 270, 629);
    lv_obj_set_style_arc_color(ui_BatteryPowerIndicator, lv_color_hex(0xC8C8C8), LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_opa(ui_BatteryPowerIndicator, 255, LV_PART_MAIN | LV_STATE_DEFAULT);
    lv_obj_set_style_arc_width(ui_BatteryPowerIndicator, 5, LV_PART_MAIN | LV_STATE_DEFAULT);

    lv_obj_set_style_arc_width(ui_BatteryPowerIndicator, 5, LV_PART_INDICATOR | LV_STATE_DEFAULT);

    lv_obj_set_style_bg_color(ui_BatteryPowerIndicator, lv_color_hex(0xFFFFFF), LV_PART_KNOB | LV_STATE_DEFAULT);
    lv_obj_set_style_bg_opa(ui_BatteryPowerIndicator, 0, LV_PART_KNOB | LV_STATE_DEFAULT);

}

void ui_init(void)
{
    lv_disp_t * dispp = lv_disp_get_default();
    lv_theme_t * theme = lv_theme_default_init(dispp, lv_palette_main(LV_PALETTE_BLUE), lv_palette_main(LV_PALETTE_RED),
                                               false, LV_FONT_DEFAULT);
    lv_disp_set_theme(dispp, theme);
    ui_SplashScreen_screen_init();
    ui_MainScreen_screen_init();
    lv_disp_load_scr(ui_SplashScreen);
}
