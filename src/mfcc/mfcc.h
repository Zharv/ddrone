#ifndef MFCC_H
#define MFCC_H

#pragma once
#include <math.h>

#define SAMPLE_RATE 44100
#define FRAME_SIZE 512
#define NUM_FILTER_BANKS 26
#define NUM_DCT_COEFFS 13
#define NUM_WINDOWS 1

class MFCC
{

public:
    float mfcc_coeffs[NUM_WINDOWS][NUM_DCT_COEFFS];

   // void begin(TFT_eSPI *display);
    void process_audio_samples(float *buffer);
    void get_average_mfcc(float *averages);

private:
    //TFT_eSPI *_tft;
    int num_windows = 0;

    void calculate_mfcc_coeffs(float *buffer, int window_index);
};

#endif // MFCC_H